<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

require_once './vendor/autoload.php';
require_once 'functions.php';
require_once 'config.php';
require_once 'src/JNKTransmitter.php';

define('SECONDS_PER_MINUTE', 60);
define('SECONDS_PER_HOUR',   SECONDS_PER_MINUTE*60);
define('SECONDS_PER_DAY',    SECONDS_PER_HOUR*24);
define('SECONDS_PER_WEEK',   SECONDS_PER_DAY*7);
define('SECONDS_PER_MONTH',  SECONDS_PER_DAY*30);
define('SECONDS_PER_YEAR',   SECONDS_PER_DAY*365);

define('AUTH_KEY_SALT', '1234');

if (isset($_POST['auth']) || (isset($_POST['mobile']) && isset($_POST['username']))) {
    $response = array(
        "loginStatus" => "",
        "username"    => "",
        "authkey"     => ""
    );

    if (isAuthenticated()) {
        $response['loginStatus'] = "Success";
        echo json_encode($response);
    } else {
        $response['loginStatus'] = authenticate() ? "Success" : "Error login in.";
        $response['username'] = $_COOKIE['username'];
        $response['authkey'] = $_COOKIE['authkey'];
        echo json_encode($response);
    }
}

// initialize transmitter
$transmitter = new JNKTransmitter(AUTH_KEY_SALT);

// get input array from iOS / OSX
$input = $transmitter->input();

// set $_POST variables used in authentication.
$_POST['username'] = getValue($input, 'username');
$_POST['password'] = getValue($input, 'password');

$input['authenticated'] = authenticate();
// send response array back to iOS / OSX
$input['password'] = '';
$transmitter->output($input);

function isAuthenticated()
{
    $username = getValue($_POST, 'username');

    if (isset($username)) {
        global $input;
        if (isset($_POST['authkey']) && ($_POST['authkey'] === getAuthKey($username))) {
            return true;
        }
    }

    return false;
}

/**
 * Autenthicate on all web services if possible
 *
 * @return [string] [authentication result]
 */
function authenticate()
{
    global $adldap;
    $adldap = createAdldap();

    if (isAuthenticated()) {
         return true;
    }

    $username = getValue($_POST, 'username');
    $password = getValue($_POST, 'password');
    $errorMessage = null;

    if (!empty($username) && !empty($password)) {
        $username = strtolower($username);

        // Authenticate on ldap server
        if ($adldap->authenticate($username, $password)) {

            $authKey = getAuthKey($username);

            // Use '/' to make these cookies available to all apps using this service
            setcookie("username", $username, time() + SECONDS_PER_DAY * 35, '/');
            setcookie("authkey", $authKey, time() + SECONDS_PER_DAY * 30, '/');

            // when a user logs in for the first time or after clearing cookies
            // the username cookie will not be set, but it is referenced later
            // so we set it here manually.
            // This is because setcookie() does not update $_COOKIE (until the next request).
            $_COOKIE['username'] = $username;
            $_COOKIE['authkey'] = $authKey;

            return true;
        }
    } else {
        return false;
    }

    return $adldap->getLastError();
}

function createAdldap()
{
    //these settings point adLDAP at the Global Catalog to prevent referrals
    return new \adLDAP\adLDAP(array('account_suffix'=>'@address.com',
    						'domain_controllers'=>array('address.com'),
    						'base_dn'=>'',
    						'ad_port'=>'1234'));
}

function getAuthKey($username)
{
    $encrypted  = crypt($username, AUTH_KEY_SALT);

    /**
     * By default the salt is prepended to the encrypted result.  Remove it.
     */

    return str_replace(AUTH_KEY_SALT, '', $encrypted);
}


/**
 * The following page was used to determine how to interface with C#'s AesManaged class in the web service:
 * http://stackoverflow.com/questions/8116133/wp-aesmanaged-encryption-vs-mcrypt-encrypt
 */
function encrypt($data)
{
    $data = addEncryptionPadding($data);
    $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, ENCRYPTION_KEY, $data, MCRYPT_MODE_CBC, ENCRYPTION_IV);

    return base64_encode($encrypted);
}

function addEncryptionPadding($string, $blocksize = 16)
{
    $len = strlen($string);
    $pad = $blocksize - ($len % $blocksize);
    $string .= str_repeat(chr($pad), $pad);

    return $string;
}
