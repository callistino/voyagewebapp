<?php

function getValue($array, $name, $default = null)
{
    return array_key_exists($name, $array) ? $array[$name] : $default;
}

/**
 * HTTP REST POST and GET with Curl
 *
 * @param  [string] $command [xml post]
 *
 * @return [string]          [xml response]
 */
function doCurlHTTP($url, $method = 'GET', $credentials = array(), $content = false)
{

    if (empty($url) || empty($method)) {
        return false;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);

    if ($method == 'POST') {\
        //curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($content));
    }
    if ($credentials !== array() && isset($credentials['username'], $credentials['password'])) {
        curl_setopt($ch, CURLOPT_USERPWD, ($credentials['username']) . ':' . ($credentials['password']));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);

    $curl['response']     = curl_exec($ch);
    $curl['error']        = curl_errno($ch);
    $curl['errmsg']       = curl_error($ch);
    $curl['header']       = curl_getinfo($ch);
    $curl['responseCode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    return $curl;

}

/**
 * @return array
 */
function getSystemsTMS()
{
    $url     = 'http://address.com';
    $client  = new SoapClient($url, array(
        "login"        => "DOMAIN\user",
        "password"     => "pass",
        "trace"        => true,
        'soap_version' => SOAP_1_2
    ));
    $phones  = array();
    $devices = $client->GetSystems();
    $devices = $devices->GetSystemsResult->TMSSystem;
    foreach ($devices as $dev) {
        if (strpos($dev->SystemType, 'SX20')) {
            $name     = $dev->SystemName;
            $mac      = substr($dev->SystemName, strpos($dev->SystemName, 'SEP'));
            $mac      = substr($mac, 0, strpos($mac, ')'));
            $ip       = substr($dev->WebInterfaceURL, 0, strpos($dev->WebInterfaceURL, '/'));
            $phones[] = array('ip_address' => $ip, 'device_id' => $mac, 'device_description' => $name);
        }
    }

    return $phones;
}

function updatePhonesByTMS()
{
    $voyagerDB = new VoyagerDB();

    try {
        $phones = getSystemsTMS();
        foreach ($phones as $phone) {
            $voyagerDB->insertOrUpdatePhone($phone);
        }

        return true;
    } catch (Exception $ex) {
        return false;
    }
}

/**
 * Capitalizes the first letter on every word on a string even if
 * separated by hyphens or apostrophes.
 *
 * @param string $string string to modify
 *
 * @return string modified string
 */
function ucname($string)
{
    $string = ucwords(strtolower($string));

    foreach (array('-', '\'') as $delimiter) {
        if (strpos($string, $delimiter) !== false) {
            $string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
        }
    }

    return $string;
}

/**
 * Validates the input data is not blank and strips any blank characters.
 *
 * @param string $data [data to validate]
 *
 * @return string [sanitized data]
 */
function checkInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}