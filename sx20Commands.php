<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);

require_once 'vendor/autoload.php';
require_once 'vendor/lastcraft/simpletest/browser.php';
require_once 'src/FAMC/Cisco/Telepresence/TelepresenceAPI.php';
require_once 'src/FAMC/Cisco/Telepresence/sx20Actions.php';
require_once 'src/FAMC/Cisco/Telepresence/MCUAPI.php';
require_once 'src/FAMC/db/VoyagerDB.php';
require_once 'config.php';
require_once 'functions.php';

use FAMC\Cisco\Telepresence\TelepresenceAPI\TelepresenceAPI;
use FAMC\Cisco\Telepresence\MCUAPI\MCUAPI;
use adLDAP\adLDAP;

$voyagerDB = new VoyagerDB();
$tAPI      = new TelepresenceAPI();
$mAPI      = new MCUAPI();
$ldap      = new adLDAP(array(
                 'account_suffix'     => '@'.LDAP_HOST,
                 'domain_controllers' => array(LDAP_HOST),
                 'base_dn'            => '',
                 'ad_port'            => LDAP_PORT
             ));

$ldap->authenticate(LDAP_USERNAME, LDAP_PASSWORD);

//experimental load test
//run every time script is called
updatePhonesByTMS();

if (isset($_POST['deviceIP'], $_POST['Command'])) {
    // Commands that need a device to execute
    $tAPI->device = checkInput($_POST['deviceIP']);
    $command = checkInput($_POST['Command']);

    switch ($command) {
        case 'Call':
            $action = actionCall($tAPI);
            break;
        case 'KeyClick':
            actionKeyClick($tAPI);
            break;
        case 'Disconnect':
            actionDisconnect($tAPI);
            break;
        case 'DisconnectAll':
            actionDisconnectAll($tAPI);
            break;
        case 'CamPosition':
            $action = actionCamPosition($tAPI,$voyagerDB);
            break;
        case 'MoveCamToLeft':
            $action = actionMoveCamToLeft($tAPI);
            break;
        case 'MoveCamToRight':
            $action = actionMoveCamToRight($tAPI);
            break;
        case 'StopCamPanMovement':
            actionStopCamPanMovement($tAPI);
            break;
        case 'MoveCamUp':
            $action = actionMoveCamUp($tAPI);
            break;
        case 'MoveCamDown':
            $action = actionMoveCamDown($tAPI);
            break;
        case 'StopCamTiltMovement':
            actionStopCamTiltMovement($tAPI);
            break;
        case 'ZoomIn':
            $action = actionZoomIn($tAPI);
            break;
        case 'ZoomOut':
            $action = actionZoomOut($tAPI);
            break;
        case 'StopZoom':
            actionStopZoom($tAPI);
            break;
        case 'MoveToPreset':
            $action = actionMoveToPreset($tAPI);
            break;
        case 'GetPresetList':
            actionGetPresetList($tAPI);
            break;
        case 'GetStatus':
            actionGetStatus($tAPI);
            break;
        case 'GetCallStatus':
            actionGetCallStatus($tAPI);
            break;
        case 'GetCallHistory':
            actionGetCallHistory($tAPI);
            break;
        case 'GetPresentationLayout':
            actionGetPresentationLayout($tAPI);
            break;
        case 'GetFavorites':
            actionGetFavorites($tAPI);
            break;
        case 'UpdateSnapshot':
            actionUpdateSnapshot($tAPI);
            break;
        case 'UpdateVolume':
            actionUpdateVolume($tAPI);
            break;
        case 'UpdatePresentation':
            actionUpdatePresentation($tAPI);
            break;
        case 'UpdateMicrophones':
            actionUpdateMicrophones($tAPI);
            break;
        case 'UpdateSelfview':
            actionUpdateSelfview($tAPI);
            break;
        case 'StartPresentation':
            $action = actionStartPresentation($tAPI);
            break;
        case 'StopPresentation':
            $action = actionStopPresentation($tAPI);
            break;
        case 'NextRemoteLayout':
            $action = actionNextRemoteLayout($tAPI);
            break;
        case 'Mute':
            $action = actionMute($tAPI);
            break;
        case 'Unmute':
            $action = actionUnmute($tAPI);
            break;
        case 'ChangeVolume':
            $action = actionChangeVolume($tAPI);
            break;
        case 'ChangeLayout':
            $action = actionChangeLayout($tAPI);
            break;
        case 'ShowSelfview':
            $action = actionShowSelfview($tAPI);
            break;
        case 'HideSelfview':
            $action = actionHideSelfview($tAPI);
            break;
        default:
            echo "Error: command not found!\n";
            print_r($_POST);
            break;
    }
} else if (isset($_POST['Command'])) {
    // Deviceless commands
    $command = checkInput($_POST['Command']);

    switch ($command) {
        case 'GetDeviceList':
            actionGetDeviceList($voyagerDB);
            break;
        case 'GetRoomsList':
            actionGetRoomsList($voyagerDB);
            break;
    }
} else {
    echo "Error: command not found!\n";
    print_r($_POST);
}

//store user command
if (isset($_POST['Username'], $action)) {
    $userDisplayName = $ldap->user()->info(($_POST['Username']), array('displayname'));
    $action = $userDisplayName[0]['displayname'][0] . " $action";
    $voyagerDB->insertUserEvent($_POST['Username'], $action, checkInput($_POST['deviceIP']));
}


