<?php
require_once 'config.php';

$lastWeek = date('Ymd',strtotime("last week")) ."T". date('His',strtotime("last week"));
$today    = date('Ymd',strtotime("today")) ."T". date('His',strtotime("today"));
$todayAtFive= date('Ymd',strtotime("today")) ."T". date('His',strtotime("4:45 pm"));
$tomorrow = date('Ymd',strtotime("tomorrow")) ."T". date('His',strtotime("tomorrow"));
$now      = date('Ymd',time()) ."T". date('His',time());

ini_set('display_errors', 1);
error_reporting(E_ALL);
// Make sure you include the autoloader so all dependencies can be loaded (see getcomposer.org)
include_once './vendor/autoload.php';
// Create a soapclient with the correct params of your ZCS server
$soapClientA = new \Zimbra\ZCS\SoapClient("calendar.com", "7071", "user", "pass");
$soapClientB = new \Zimbra\ZCS\SoapClient("calendar.com", "7071", "user", "pass");

$user     = new \Zimbra\ZCS\User\Account($soapClientA);
$calendar = new \Zimbra\ZCS\Mail\Account($soapClientB);


if (0){
    echo '<pre>';
    $data = listReacurringAppointmentsByUsername($user, $calendar, 'user','');


    var_dump($data);
    echo '</pre>';
}

if(0){
    echo '<pre>';
    $data = $calendar->getAppointment('UUID');
    var_dump($data);
    echo '</pre>';
}


if(0){//test create
    $_POST['createApp']   = true;
    //$_POST['appStart']    = '20130912T063000';
    //$_POST['appStart']    = $todayAtFive;
    $_POST['appStart']    = 1379364614;
    $_POST['appDuration'] = 10;//minutes
    $_POST['appTitle']    = 'Voyager Unix time stamp test';
    $_POST['appDesc']     = 'Ignore this test meeting';
    $_POST['appLocation'] = '6thfloordevconferenceroom';
    $_SERVER['REQUEST_METHOD'] = 'POST';
}
if (0){//test delete
    $_POST['appUUID'] = '4220a59d-af25-431d-a11e-3b826ae0e312';
    $_POST['cancelAppByUUID'] = true;
    $_SERVER['REQUEST_METHOD'] = 'POST';

}


if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (isset($_POST['getCal'])){
        $data = $cal = $calendar->getCal();
    }else if(isset($_POST['getApps'],$_POST['appStart'],$_POST['appEnd'])){
        $data = $calendar->getAppointments(convertUnixTimeToZimbra($_POST['appStart']),$_POST['appEnd']);
    }else if(isset($_POST['getAppById'],$_POST['appId'])){
        $data = $calendar->getAppointment($_POST['appId']);
    }else if(isset($_POST['cancelAppByUUID'],$_POST['appUUID'])){

        $data = $calendar->cancelAppointmentSeriesByUUID($_POST['appUUID']);
    }else if(isset($_POST['createApp'],$_POST['appStart'],$_POST['appDuration'],$_POST['appTitle'],$_POST['appDesc'],
    $_POST['appLocation'])){
        print_r($_POST);
        $unixTime = $_POST['appStart'];
        if ($unixTime < time() ){
            $unixTime = time();
        }
        if (!is_numeric($unixTime)){
            die('Start Time Error: not a numeric unix timestamp');
        }
        if (is_numeric($_POST['appDuration']) && ($_POST['appDuration']<=120) ){
            $duration = $_POST['appDuration'] * 60;
        }else{
            $duration = 15 * 60;
        }
        
        $start = convertUnixTimeToZimbra($unixTime);
        $unixTime = $unixTime + $duration;
        $end      = convertUnixTimeToZimbra($unixTime);
        $data     = $calendar->createAppointment($start,$end,$_POST['appTitle'],
                                                 $_POST['appDesc'],$_POST['appLocation']);

    }
    die('true');
}

function convertUnixTimeToZimbra($unixTime){
    return date('Ymd',$unixTime)."T".date('His',$unixTime);
}
function listReacurringAppointmentsByUsername($user,$calendar,$username='voyager',$search=false){

    $userID   = $user->getAccountIDByUsername($username);
    //var_dump($userID);
    $start = time() * 1000;
    //var_dump($start);
    $xml      = $calendar->getAppointmentsByUserID($start,$userID, $search,$username);
    echo "\n***************************************************\n";

    $response = array();
    
    $appIDs = array();
    foreach ($xml->appt as $x){
            echo $x->attributes()->name . "\n";
            $id   = (String)$x->attributes()->uid;

            $name = (String)$x->attributes()->name;
            $app = $calendar->getAppointment($id);
            echo "\n$id  \n==================\n";

            $appIDs[$name] = $id;
    }
    return $appIDs;
}

function xml2array($xml)
{
    $arr = array();
    foreach ($xml as $element)
    {
        $tag = $element->getName();
        $e = get_object_vars($element);
        $attr = $element->attributes();
        if (!empty($e)){
            $arr[$tag] = array($element instanceof SimpleXMLElement ? xml2array($element) : $e, $attr);
        }else{
            $arr[$tag] = array(trim($element),$attr);
        }
    }
    return $arr;
}
