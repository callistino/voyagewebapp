<?php
function mobileHTMLTeamScrums($events){
    $html = "<div class='panel-group' id='accordion'>";
    foreach($events as $key => $event){
        if($event['DTSTART'] > strtotime('today') && $event['DTSTART'] < strtotime('tomorrow')){
            $arr [$key]= $event['DTSTART'];
        }
    }
    asort($arr);
    foreach($arr as $key =>$val ){
        $event = $events[$key];
        $attendees='';
        $desc = (String)$event['DESCRIPTION'];
        $desc = str_replace('-','',$desc);
        foreach($event['ATTENDEES'] as $a){
           $attendees .= $a['CN'] . " - " . $a['EMAIL']. "<br>";
        }
        $phoneNumber = findPhoneNumber($desc);
        if (!empty($phoneNumber)){
            $phoneNumber = str_replace(' ', '',$phoneNumber);
            $phoneIcon   = "<a href='tel:$phoneNumber' type='button' class='btn icon-phone btn-inline pull-left'></a>";
            $phoneNumber = "<a href='tel:$phoneNumber'><h5 class='panel-title'>Call $phoneNumber</h5></a>";
        }
        if ($event['DTSTART'] < time() && $event['DTEND'] < time()){
            $panelClass = 'color8';
        }else if($event['DTSTART'] < time() && $event['DTEND'] > time()){
            $panelClass = 'color4';
        }else{
            $panelClass = '';
        }
        $html .= "
          <div class='panel $panelClass'>
          $phoneIcon
            <div class='panel-heading'>
              <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#collapse$key'>
              <h4 class='panel-title'>
                  $event[SUMMARY]<br>".
                    date('g:ia', $event['DTSTART']) .
                    " to ".
                    date('g:ia', $event['DTEND']) .
             "</h4></a> $phoneNumber
            </div>
            <div id='collapse$key' class='panel-collapse collapse'>
              <div class='panel-body'>
                Location: $event[LOCATION] <br>
                Description: $event[DESCRIPTION]<br>
                Attendees: $attendees
              </div>
            </div>
          </div>";
    }
    return $html . "</div>";
}

function mobileHTMLNotificationHistory($history){
    $html = '<ul class="list-group">';
    foreach($history as $event){
        $html .= "<li class='list-group-item'>$event[DISPLAY_NAME] - "."$event[EVENT]<br>".
            date('m-d G:ia', $event['EVENT_TIME']) ;
    }
    $html .= '</ul>';
    return $html;
}
  
function findPhoneNumber ($searchString) {
    $pos = strpos($searchString, (String)REMOTE_SCRUM_PREFIX);
    $phoneNumber = '';
    if ($pos !== false) {
        $chars = str_split(substr($searchString, $pos));
        foreach ($chars as $char) {
            if (ctype_alpha ($char)) {
                break;
            } else {
                $phoneNumber .= $char;
            }
        }
    }

    return $phoneNumber;
}