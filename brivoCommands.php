<?php
require_once 'vendor/autoload.php';
require_once 'src/FAMC/db/VoyagerDB.php';
require_once 'config.php';
require_once 'functions-mobile.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
if (isset($_POST['doorFormId'])){
  $door = trim($_POST['doorFormId']);
}else{
  //temporary use main door
  $door = 'door';
}

//TODO add user authentication check and log each request

$connection = new AMQPConnection(RABBITMQ_HOST, RABBITMQ_PORT,
                                 RABBITMQ_USER, RABBITMQ_PASSWORD);
$channel = $connection->channel();
$channel->queue_declare(RABBITMQ_QUEUE, false, false, false, false);
$msg = new AMQPMessage($door);
$channel->basic_publish($msg, '', RABBITMQ_QUEUE);
echo " \n";
$channel->close();
$connection->close();
