/**
 * @author Reinier Pelayo
 *
 */
Device = (function () {
    __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    function Device () {
        this.inCall = false;
        this.callId = undefined;
        this.dialedNumber = undefined;
        this.presentationOn = false;
        this.presentationLayout = undefined;
        this.current = undefined;
        this.micStatus = undefined;
    }

    return Device;
})();
