var list        = ['pulseDoor2153917Form', '6thITStorageCloset' , '6thITSwitchRoom'],
    listButtons = $('#brivoPage .list-group-item');

$(document).ready(function () {
    'use strict';

    // List buttons
    listButtons.on('touchend', openDoorByName);
});

function openDoorByName (event) {
    'use strict';

    var button   = $(event.currentTarget),
        index    = listButtons.index(button),
        doorName = list[index];

    // Request open from API
    $.post(app.config.server + app.config.brivoPath, {
        mobile:     true,
        username:   app.currentUser,
        Command:    'openDoor',
        doorFormId: doorName
    }).done(function (data) {
    });
}