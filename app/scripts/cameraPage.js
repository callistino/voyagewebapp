$(document).ready(function () {
    'use strict';

    // Camera action button handlers
    $('#btnCamUp').on('touchend',cameraAction);
    $('#btnCamDown').on('touchend',cameraAction);
    $('#btnCamLeft').on('touchend',cameraAction);
    $('#btnCamRight').on('touchend',cameraAction);
    $('#btnZoomIn').on('touchend',cameraAction);
    $('#btnZoomOut').on('touchend',cameraAction);

    // Hide cam presets button on load and only show if there are any presets
    $('#btnCamPresets').hide();

    // Show list of camera presets
    $('#btnCamPresets').on('touchend', function (event) {
        event.stopImmediatePropagation();
        loadCameraPresets();
        $('#presetsModal').modal('show');
    });

    // Disable camera buttons if not connected to a device
    $('#cameraPage button').prop('disabled', true);
});

function loadCameraPreview (data) {
    'use strict';

    if (!app.device.current || $('#cameraPage:visible').length <= 0) {
        return false;
    }

    if (data && data.replace('data:image/png;base64,', '') !== '') { // Only update snapshot if we receive data
        $('#cameraPreview img').attr('src', data);
    } else {
        $('#cameraPreview img').attr('src', 'images/voyager.png');
    }
}

function loadCameraPresets () {
    'use strict';

    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Username: app.currentUser,
        Command: 'GetPresetList',
        deviceIP: app.device.current.ip_address
    }).done(function (data) {
        if (data) {
            $('#presetsModal .modal-body').html(data);
            $('#btnCamPresets').show();
        } else {
            $('#presetsModal').modal('hide');
            $('#btnCamPresets').hide();
        }
    });
}

function onPresetClick (element) {
    'use strict';

    var presetsLi = $('#presetsModal .list-group-item');

    if (!$(element).hasClass('active')) {
        presetsLi.removeClass('active');
    }
    $(element).toggleClass('active');

    moveCamToPreset(presetsLi.index(element) + 1);
    $('#presetsModal').modal('hide');
}

function moveCamToPreset (preset) {
    'use strict';

    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Username: app.currentUser,
        Command: 'MoveToPreset',
        presetNumber: preset,
        camId: 1,
        deviceIP: app.device.current.ip_address
    }).done(function (data) {
        var result = $.parseJSON(data);
        app.snapshotSync.update();
    });
}

function cameraAction (e) {
    'use strict';

    if (!app.device.current) {
        return false;
    }

    var button = $(e.currentTarget),
        panIncrement = 100,
        tiltIncrement = 50,
        zoomIncrement = 200;

    var arr = {
        Username: app.currentUser,
        deviceIP: app.device.current.ip_address,
        Command: 'CamPosition',
        camId: app.camera.id,
        Pan: +app.camera.position.pan,
        Zoom: +app.camera.position.zoom,
        Tilt: +app.camera.position.tilt
    };
    switch (button.prop('id')) {
        case 'btnCamUp':
            arr.Tilt += tiltIncrement;
            app.camera.position.tilt = +app.camera.position.tilt + tiltIncrement;
            break;
        case 'btnCamDown':
            arr.Tilt -= tiltIncrement;
            app.camera.position.tilt = app.camera.position.tilt - tiltIncrement;
            break;
        case 'btnCamLeft':
            arr.Pan -= panIncrement;
            app.camera.position.pan = +app.camera.position.pan - panIncrement;
            break;
        case 'btnCamRight':
            arr.Pan += panIncrement;
            app.camera.position.pan = +app.camera.position.pan + panIncrement;
            break;
        case 'btnZoomIn':
            arr.Zoom += zoomIncrement;
            app.camera.position.zoom = +app.camera.position.zoom + zoomIncrement;
            break;
        case 'btnZoomOut':
            arr.Zoom -= zoomIncrement;
            app.camera.position.zoom = +app.camera.position.zoom - zoomIncrement;
            break;
    }

    $.post(app.config.server + app.config.ciscoPath, arr).done(app.snapshotSync.update);
}


