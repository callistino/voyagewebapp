$(document).ready(function () {
    'use strict';

    // Tools page handlers
    $('.device-notifications').on('touchend', showNotificationsPage);
    $('.device-calendar').on('touchend', showCalendarPage);
    $('#checkSelfNotifications').on('change', toggleSelfNotifications);
    $('.device-sync').on('touchend', refreshData);
});

function toggleSelfNotifications (touchEvent) {
    'use strict';

    touchEvent.stopPropagation();
    app.notificationsSync.data.User = "";
    app.notificationsSync.jqXHR.abort();
    app.showSelfNotifications = touchEvent.target.checked;
}

function showCalendarPage () {
    'use strict';
    /* global Calendar */

    if (!app.device.current) {
        bootbox.alert("Please select a device");
        return false;
    }

    $('.pt-pages').hide();
    $('#calendarPage').show();
    $('#btnHomePage').hide();
    $('#btnPrevPage').show()

    $('#calendarPage').trigger('eventBuildCalendar');

    app.prevPage = app.currPage;
    app.currPage = 'calendarPage';
}

function showNotificationsPage () {
    'use strict';

    if (!app.device.current) {
        bootbox.alert("Please select a device.");
        return false;
    }

    $('.pt-pages').hide();
    $('#notificationsPage').html('').show();

    startLoading('notifications');

    $.post(app.config.server + app.config.notificationPath, {
        mobile: true,
        GetNotificationHistory: true,
        DeviceIP: app.device.current.ip_address
    }).done(function (data) {
        $('#notificationsPage').html(data).show();
        $('#btnHomePage').hide();
        $('#btnPrevPage').show();
        app.prevPage = app.currPage;
        app.currPage = 'notificationsPage';
        if (!data) {
            bootbox.alert('There are currently no notifications', function () {
                pushPrevPage();
                return;
            });
        }
    });
}

function loadMultiCalendar (element) {
    'use strict';

    var listItem = $(element),
    roomIds = [],
    locationListElement = $('.locations-list'),
    calendarElement = $('.locations-calendar');

    listItem.parent().children().filter('.selected').each(function () {
        roomIds.push($(this).prop('id').trim());
    });

    // Open or add another calendar timeline
    if (roomIds.length !== 0) {
        locationListElement.addClass('locations-list-open');
        calendarElement.addClass('locations-calendar-open');

        calendarElement.find('.locations-calendar-timeline').remove();
        $.each(roomIds, function (key, value) {
            var timeline = $('<div>', {
                'class': 'locations-calendar-timeline',
                'id': 'timeline-' + value
            });
            calendarElement.append(timeline);
            loadCalendarTimeline(timeline);
        });
    } else { // Close timeline
        locationListElement.removeClass('locations-list-open');
        calendarElement.removeClass('locations-calendar-open');
    }
}

