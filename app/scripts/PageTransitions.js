var PageTransitions = (function () {

	var main    		  = $('.pt-pages'),
		pages      		  = main.children('div.page'),
		pagesCount 		  = pages.length,
		current    		  = 0,
    	next              = 0,
		isAnimating 	  = false,
		endCurrPage 	  = false,
		endNextPage 	  = false,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		// animation end event name
		animEndEventName  = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		// support css animations
		support 	      = Modernizr.cssanimations;

	function init () {

		pages.each(function () {
			var page = $(this);
			page.data('originalClassList', page.attr('class'));
		});

		pages.eq(current).addClass('pt-page-current');

        // Tabbar buttons highlight
        $('.tabbar-item').on('touchend', function(ev) {
            var el = $(ev.currentTarget);

        	if (!app.device.current) return;		// Do not transition to other pages
        	if (el.hasClass('active')) return; 	// Do not transition on currently active

            ev.preventDefault();
            next = $('.tabbar-item').index(el);
            nextPage(el.attr('data-animation'));

            pages.each(function () {
                var page 	    = $(this),
                    tabbarItems = $('.tabbar-item'),
                    pageIndex   = pages.index(page),
                    tabIndex    = tabbarItems.index(el);

                if (pageIndex < tabIndex) {
                    tabbarItems.eq(pageIndex).attr('data-animation', 'pushPageRight');
                } else {
                    tabbarItems.eq(pageIndex).attr('data-animation', 'pushPageLeft');
                }
            });
		});
	}

	function nextPage (animation) {

		if (isAnimating) {
			return false;
		}

		isAnimating = true;

		var currPage = pages.eq(current),
		    nextPage = pages.eq(next).addClass('pt-page-current'),
			outClass = '', inClass = '';

		current = next;

		switch(animation) {
			case "pushPageLeft":
				outClass = 'pt-page-fade';
				inClass = 'pt-page-moveFromRight pt-page-ontop';
				break;
			case "pushPageRight":
				outClass = 'pt-page-fade';
				inClass = 'pt-page-moveFromLeft pt-page-ontop';
				break;
		}

		currPage.addClass(outClass).on(animEndEventName, function () {
			currPage.off(animEndEventName);
			endCurrPage = true;
			if (endNextPage) {
				onEndAnimation(currPage, nextPage);
			}
		});

		nextPage.addClass(inClass).on(animEndEventName, function () {
			nextPage.off(animEndEventName);
			endNextPage = true;
			if (endCurrPage) {
				onEndAnimation(currPage, nextPage);
			}
		} );

		if (!support) {
			onEndAnimation(currPage, nextPage);
		}

	}

	function onEndAnimation (outpage, inpage) {
		endCurrPage = false;
		endNextPage = false;
		resetPage(outpage, inpage);
		isAnimating = false;
	}

	function resetPage (outpage, inpage) {
		outpage.attr('class', outpage.data('originalClassList'));
		inpage.attr('class', inpage.data('originalClassList') + ' pt-page-current');
	}

	init();

	return { init : init };

})();
