/**
 * @author Reinier Pelayo
 *
 */
Camera = (function () {
    __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    function Camera () {
        this.id = 1;
        this.connected = false;
        this.model = '';
        this.selfviewOn = false;
        this.position = {
            'pan': '0',
            'tilt': '0',
            'zoom': '0'
        };
    }

    Camera.prototype.moveToPosition = function (pan, tilt, zoom) {
        'use strict';

        this.position.pan = pan;
        this.position.tilt = tilt;
        this.position.zoom = zoom;

        $.post(app.config.server + app.config.ciscoPath, {
            deviceIP: app.device.current.ip_address,
            Command: 'CamPosition',
            camId: this.id,
            Pan: this.position.pan,
            Tilt: this.position.tilt,
            Zoom: this.position.zoom,
            Username: app.currentUser
        }).done(function (data) {
            app.ajaxResult = data;
        });
    };

    return Camera;
})();

