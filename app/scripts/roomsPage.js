function loadMeetingModal (id) {
    'use strict';

    var button = $(id),
    listItem = button.parent();

    $('#roomsModal').modal('show');
    $('#roomsModalTitle').html(listItem.text());
    $('#roomsModalID').val(listItem.prop('id'));
}

function addMeeting (id) {
    'use strict';

    var roomId      = $('#roomsModalID').val(),
        meetingTime = $('.lbl-meeting-time').text(),
        roomsModal  = $('#roomsModal');

    startLoading('roomsModalTitle');
    $.post(app.config.server + app.config.calendar, {
        //mobile: true,
        createApp: true,
        appLocation: roomId,
        appStart: 0,
        appDuration: meetingTime,
        appTitle: 'Time slot reserved by Voyager',
        appDesc: 'Time slot reserved by Voyager '
    }).done(function (data) {
        roomsModal.modal('hide');
    });
}

function changeMeetingTime (button) {
    'use strict';

    var timeAmount = 15,
    newTime = 0,
    meetingTimeLabel = $('.lbl-meeting-time');
    button = $(button);

    switch (button.text()) {
        case '+':
            meetingTimeLabel.text(+meetingTimeLabel.text() + timeAmount);
            break;
        case '-':
            meetingTimeLabel.text(+meetingTimeLabel.text() - timeAmount);
            break;
    }

    if (meetingTimeLabel.text() < timeAmount) {
        meetingTimeLabel.text(timeAmount);
    }
}


