/**
 * Copied and modified from cico SX20 web interface
 *
 * @param  string   postUrl
 * @param  number   interval [how long in ms to set the interval]
 * @param  function callback [callback with the data from response]
 *
 */
AjaxUpdater = (function() {
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  function AjaxUpdater(postUrl, data, interval, callback) {
    this.postUrl    = postUrl;
    this.data       = data;
    this.interval   = interval;
    this.callback   = callback;
    this.onComplete = __bind(this.onComplete, this);
    this.onSuccess  = __bind(this.onSuccess, this);
    this.onError    = __bind(this.onError, this);
    this.update     = __bind(this.update, this);
    this.stop();
  }

  AjaxUpdater.prototype.show_error = function (text) {
    this.elem.css('background', 'url("/static/images/no-snapshot.png") ' + 'no-repeat scroll center center #CCCCCC');
  };

  AjaxUpdater.prototype.stop = function() {
    return this.run = false;
  };

  AjaxUpdater.prototype.start = function() {
    this.run = true;
    return this.update();
  };

  AjaxUpdater.prototype.update = function() {
    return this.jqXHR = $.ajax({
      type: 'POST',
      url: this.postUrl,
      data: this.data,
      error: this.onError,
      success: this.onSuccess,
      complete: this.onComplete
    });
  };

  AjaxUpdater.prototype.onError = function(xhr, status, exc) {
    if (this.callback) {
      return this.callback('', status, xhr);
    }
  };

  AjaxUpdater.prototype.onSuccess = function(data, status, xhr) {
    if (this.callback) {
      return this.callback(data, status, xhr);
    }
  };

  AjaxUpdater.prototype.onComplete = function(xhr, text) {
    if (this.run) {
        return setTimeout(this.update, this.interval);
    }
  };

  return AjaxUpdater;

})();
