var Calendar = (function () {
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

    function Calendar () {
        this.firstDate    = new Date();
        this.lastDate     = new Date();
        this.currentDate  = new Date().midnight();
        this.dayListBuilt = false;

        this.numberOfDays         = __bind(this.numberOfDays, this);
        this.dateAtIndex          = __bind(this.dateAtIndex, this);
        this.currentDateIndex     = __bind(this.currentDateIndex, this);
        this.generateDaysListHtml = __bind(this.generateDaysListHtml, this);
    }

    Calendar.prototype.numberOfDays = function () {
        'use strict';
        var startDateUTC = Date.UTC(this.firstDate.getYear(), this.firstDate.getMonth(), this.firstDate.getDate());
        var endDateUTC = Date.UTC(this.lastDate.getYear(), this.lastDate.getMonth(), this.lastDate.getDate());
        return (endDateUTC - startDateUTC) / 86400000;
    };

    Calendar.prototype.dateAtIndex = function (index) {
        'use strict';
        if (this.numberOfDays() <= 0) {
            return 0;
        } else {
            var date = new Date(this.firstDate.getTime());
            date.setDate(date.getDate() + index);
            return date;
        }
    };

    Calendar.prototype.currentDateIndex = function () {
        'use strict';
        if (this.numberOfDays() > 0) {
            return this.numberOfDays();
        } else {
            return 0;
        }
    };

    Calendar.prototype.generateDaysListHtml = function() {
        'use strict';

        // Create and instantiate calendar object
        var calendarDays = $('.calendar-day-scroller'),
            cellWidth    = 70,
            totalWidth   = cellWidth * this.numberOfDays(),
            weekendDays  = 0,
            weekendWidth = 0,
            dayButton    = $('<button type="button" class="btn btn-calendar-day">'),
            blankButton  = $('<button type="button" class="btn btn-calendar-day btn-calendar-day-blank">'),
            btnName      = $('<span class="calendar-day-name">'),
            btnNumber    = $('<span class="calendar-day-number">'),
            html         = $('<div class="btn-group">'); 

        for (var i = 0; i < this.numberOfDays(); i++) {
            var date      = this.dateAtIndex(i),
                day       = date.getDayName(),
                dayNumber = date.getDate();

            if (day === 'Saturday' || day === 'Sunday') {
                weekendDays++;
                html.append(blankButton.clone());
            } else {
                dayButton.clone().append(
                    btnName.clone().text(day.substr(0, 3)), 
                    btnNumber.clone().text(dayNumber)
                ).appendTo(html);
            }

            if (this.currentDate.getTime() === date.midnight().getTime()) {
                $(html).find('.btn-calendar-day:last').addClass('highlight today');
            }
        }

        weekendWidth = cellWidth * weekendDays;
        html.css('width', (totalWidth - weekendWidth) + 'px');
        calendarDays.html(html);
        this.dayListBuilt = true;

        // Scroll today into view
        $('.calendar-day-scroller .highlight')[0].scrollIntoView();
        var newLeft = (-calendarDays.width()) / 2 + cellWidth / 2;
        var translateLeft = 'translate(' + newLeft + 'px, 0)';
        $('.btn-group').addClass('animate-all').css({
           'webkit-transform': translateLeft,
           'transform': translateLeft
        });
    };

    return Calendar;
})();

var calendar = new Calendar();

$('#calendarPage').on('eventBuildCalendar', function () {
    'use strict';

    // Load today events
    var room = app.rooms[app.device.current.description];

    if (room) {
        $.post(app.config.server + app.config.calendarPath, {
            mobile: true,
            GetMeetings: true,
            FromUser: room.room_id
        }).done(function (data) {
            room.events = $.parseJSON(data);
            loadDeviceMeetings();
        });
    } else {
        bootbox.alert('This room/device does not have any meetings scheduled', function () {
            pushPrevPage();
            return;
        })
    }

    if (calendar.dayListBuilt) {
        return;
    }

    // Set calendar properties.
    calendar.firstDate.firstDayOfPreviousMonth();
    calendar.lastDate.lastDayOfNextMonth();

    // Generate list of days
    calendar.generateDaysListHtml();

    // Add handler for calendar buttons to change date
    $('.btn-group').on('touchend', '.btn-calendar-day', function () {return dateChanged(room);});
});

function loadDeviceMeetings () {
    'use strict';

    var meetingsList = $('<div/>').addClass('list-group'),
        calendarDayEvents = $('.calendar-day-events'),
        events = app.rooms[app.device.current.description].events;

    for (var i in events) {
        var listGroup = $('<a>', {
            href: '#',
            'class': 'list-group-item'
        });

        var meetingName = $('<p>', {
            'class': 'meeting-title',
            html: events[i].SUMMARY
        }).append('<br />');

        var location = '&nbsp;&nbsp;' + events[i].LOCATION;
        var meetingLocation = $('<p>', {
            'class': 'meeting-location icon-appbarlocationround',
            html: location
        }).append('<br />');

        var start = new Date(events[i].DTSTART * 1000);
        var end = new Date(events[i].DTEND * 1000);
        var timeString = '&nbsp;&nbsp;' + start.getFormattedTime() + ' - ' + end.getFormattedTime();
        var meetingTime = $('<p>', {
            'class': 'meeting-time icon-appbarclock',
            html: timeString
        }).append('<br />');

        listGroup.append(meetingName, [meetingLocation, meetingTime]);
        meetingsList.append(listGroup);
    }

    calendarDayEvents.html(meetingsList);
}

function createDate (d) {
    'use strict';

    if (d === false) {
        d = new Date();
    }
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var hour = d.getHours();
    var minute = d.getMinutes();
    var second = d.getSeconds();
    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;

    return output;
}

function loadCalendarTimeline (item) {
    'use strict';

    var id = item.prop('id').replace('timeline-', '');
//    startLoading(item.prop('id'));

    // Load today events
    $.post(app.config.server + app.config.calendarPath, {
        mobile: true,
        GetMeetings: true,
        FromUser: id
    }).done(function (data) {
        var meetings = $.parseJSON(data);

        for (var meeting in meetings) {
            var start = new Date(meeting.DTSTART),
            end = new Date(meeting.DTEND),
            duration = new Date(meeting.DURATION);


        }
//        spinner.stop();
    });
}


function generateDaysList () {
    'use strict';

    for (var i = 0; i < calendar.numberOfDays(); i++) {
        var date = calendar.dateAtIndex(i);
        var day = date.getDayName();
        var dayNumber = date.getDate();

        if (day === 'Saturday' || day === 'Sunday') {
            calendarHtml.append('<button type="button" class="btn btn-calendar-day btn-calendar-day-blank"></button>');
        } else {
            calendarHtml.append(
            '<button type="button" class="btn btn-calendar-day">' +
            '<span class="calendar-day-name">' +
            day.substr(0, 3) +
            '</span>' +
            '<span class="calendar-day-number">' +
            dayNumber +
            '</span>' +
            '</button>');
        }

        if (calendar.currentDate.getTime() === date.midnight().getTime()) {
            $(calendarHtml).find('.btn-calendar-day:last').addClass('highlight today');
        }
    }
}

function dateChanged (room) {
    'use strict';

    if (!room) {
        return;
    }

    if ($(calendar).hasClass('highlight')) {
        $(calendar).removeClass('highlight');
    } else {
        $('.btn-calendar-day').removeClass('highlight');
        $(calendar).addClass('highlight');
    }

    // Load date meetings
    var date = calendar.dateAtIndex($('.btn-calendar-day.highlight').index()).midnight();
    var tomorrow = new Date(date.getTime()).midnightTomorrow();
    $.post(app.config.server + app.config.calendarPath, {
        mobile: true,
        GetMeetings: true,
        Start: (date.getFullYear() + '-' + parseInt(date.getMonth() + 1) + '-' + date.getDate()),
        End: (tomorrow.getFullYear() + '-' + parseInt(tomorrow.getMonth() + 1) + '-' + tomorrow.getDate()),
        FromUser: room.room_id
    }).done(function (data) {
        room.events = $.parseJSON(data);
        loadDeviceMeetings();
    });
}
