var conferences      = [],
    page             = $('#conferenceControlPage'),
    element 		 = page.find('.mcu-participants'),
    participantsList = {};

element.on('doneMCURequest', function () {
	page.find('.spinner').remove();
	if (!element.html()) {
		bootbox.alert('There are currently no active conferences', function () {
			$('#btnHomePage').trigger('touchend');
		});
	}
});

function fetchMCUActiveConferences () {
    'use strict';

    participantsList =  new MCUParticipantList({
        element: element, 
        allParticipants: true
    });
}
