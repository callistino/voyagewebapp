Date.prototype.monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

Date.prototype.dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Saturday'];

Date.prototype.getFormattedTime = function () {
    'use strict';
    return this.toLocaleTimeString().replace(/:\d+ /, ' ');
};

Date.prototype.getMonthName = function () {
    'use strict';
    return this.monthNames[this.getMonth()];
};

Date.prototype.getDayName = function () {
    'use strict';
    return this.dayNames[this.getDay()];
};

Date.prototype.oneDayAfter = function () {
    'use strict';
    this.setDate(this.getDate() + 1);
    return this;
};

Date.prototype.oneDayBefore = function () {
    'use strict';
    this.setDate(this.getDate() - 1);
    return this;
};

Date.prototype.midnightOfDate = function () {
    'use strict';
    this.setHours(0, 0, 0, 0);
    return this;
};

Date.prototype.midnight = function () {
    'use strict';
    this.date = new Date();
    this.setHours(0, 0, 0, 0);
    return this;
};

Date.prototype.midnightTomorrow = function () {
    'use strict';
    this.midnight().oneDayAfter();
    return this;
};

Date.prototype.midnightYesterday = function () {
    'use strict';
    this.midnight().oneDayBefore();
    return this;
};

Date.prototype.firstDayOfMonth = function () {
    'use strict';
    this.setDate(1);
    return this;
};

Date.prototype.firstDayOfNextMonth = function () {
    'use strict';
    this.setMonth(this.getMonth() + 1, 1);
    return this;
};

Date.prototype.firstDayOfPreviousMonth = function () {
    'use strict';
    this.setMonth(this.getMonth() - 1, 1);
    return this;
};

Date.prototype.firstDayOfYear = function () {
    'use strict';
    this.setMonth(0, 1);
    return this;
};

Date.prototype.firstDayOfNextYear = function () {
    'use strict';
    this.firstDayOfYear().setFullYear(this.getFullYear() + 1);
    return this;
};

Date.prototype.firstDayOfPreviousYear = function () {
    'use strict';
    this.firstDayOfYear().setFullYear(this.getFullYear() - 1);
    return this;
};

Date.prototype.lastDayOfYear = function () {
    'use strict';
    this.setMonth(11, 31);
    return this;
};

Date.prototype.lastDayOfNextYear = function () {
    'use strict';
    this.lastDayOfYear().setFullYear(this.getFullYear() + 1);
    return this;
};

Date.prototype.lastDayOfPreviousYear = function () {
    'use strict';
    this.lastDayOfYear().setFullYear(this.getFullYear() - 1);
    return this;
};

Date.prototype.lastDayOfNextMonth = function () {
    'use strict';
    this.firstDayOfNextMonth().firstDayOfNextMonth().oneDayBefore();
    return this;
};

Date.prototype.nextMonth = function () {
    'use strict';
    var month = this.getMonth();
    month = (month === 11) ? 0 : month + 1;
    this.setMonth(month);
};
