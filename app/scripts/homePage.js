$(document).ready(function () {
    'use strict';

    // Home Buttons
    $('#btnDeviceMainPage').on('touchend', pushDeviceMainPage);
    $('#btnConferenceControlPage').on('touchend', pushConferenceControlPage);
    $('#btnItScrumsPage').on('touchend', pushItScrumsPage);
    $('#btnBrivoPage').on('touchend', pushBrivoPage);
    $('#btnRoomsPage').on('touchend', pushRoomsPage);

    // Retrieve list of rooms
    $.post(app.config.server + app.config.ciscoPath, {Command: 'GetRoomsList'}).done(function (data) {
        app.rooms = $.parseJSON(data);
    });

    // Authenticate
    $.post(app.config.server + app.config.authPath, {
        auth: true,
        username: $.cookie('username'),
        authkey: $.cookie('authkey')
    }).done(function (data) {
        var response = $.parseJSON(data);
        if (response.loginStatus !== 'Success') {
            showLoginForm();
        }
    });

    // Save current user
    app.currentUser = ($.cookie('username')) ? $.cookie('username') : app.currentUser;
});

function showLoginForm () {
    'use strict';

    var loginModal = $('#loginModal'),
        loginUser = $('.login-user'),
        loginPass = $('.login-password'),
        inputs = loginUser.add(loginPass),
        loginSubmit = $('.login-submit'),
        errorAlert = $('#loginModal .modal-body .alert');

    // Init login modal
    loginModal.modal({
        backdrop: 'static',
        keyboard: false
    });

    // show login modal
    loginModal.modal('show');

    // Hide login errors alert on show
    errorAlert.hide();

    // Disable login button until both user and pass inputs are not empty
    if (!loginUser.val() || !loginPass.val()) {
        loginSubmit.prop('disabled', true);
    }

    inputs.on('input', function () {
        errorAlert.slideUp();
        if (!loginUser.val() || !loginPass.val()) {
            loginSubmit.prop('disabled', true);
        } else {
            loginSubmit.prop('disabled', false);
        }
    });

    // Handle login form submition
    $('#loginModal form').submit(function (e) {
        e.preventDefault(); // Do not refresh page

        $.post(app.config.server + app.config.authPath, {
            mobile: true,
            username: loginUser.val(),
            password: loginPass.val()
        }).done(function (data) {
            var response = $.parseJSON(data);

            if (response.loginStatus === 'Success') {
                $('#loginModal').modal('hide');
                app.currentUser = loginUser.val();

                // Set cookies
                $.cookie('username', response.username, { expires: 30, path: '/' });
                $.cookie('authkey', response.authkey, { expires: 30, path: '/' });
                $.cookie('roles', response.roles, { expires: 30, path: '/' });

                // Reload UI permissions
                flushPermissions();
            } else {
                errorAlert.html('<strong>Login error: </strong>' + response.loginStatus);
                errorAlert.slideDown();
            }
        });
    });
}

function logout () {
    'use strict';

    bootbox.confirm('Confirm Logout!', function (result) {
        if (result) {
            // Delete cookie and reset current user
            $.cookie('authkey', null, {path: '/'});
            app.currentUser = '';
            showLoginForm();
        }
    });
}

function pushDeviceMainPage (event) {
    'use strict';

    pushSecondaryPage(event);

    // Modify header
    $('.home-label').hide();
    $('#deviceListPopover').show();

    // Stop snapshot updater
    if (app.snapshotSync.run) {
        app.snapshotSync.stop();
    }

    if (!app.device.current) {
        // Timeout out popover opening or it would show before the page is
        // transitioned in.
        setTimeout(function () {
            $('#deviceListPopover').popover('show');
        }, 600);

        $('.tabbar-item').not('.active').addClass('tabbar-item-inactive');
        (event) ? event.stopPropagation() : false ;
    }

    resizeElements();
}

function pushConferenceControlPage (event) {
    'use strict';

    pushSecondaryPage(event, 'conferenceControlPage', false);
    startLoading('conferenceControlPage');
    fetchMCUActiveConferences();
}

function pushBrivoPage (event) {
    'use strict';

    pushSecondaryPage(event, 'brivoPage', false);
}

function pushItScrumsPage (event) {
    'use strict';

    pushSecondaryPage(event, 'itScrumsPage', false);

    startLoading('itScrumsPage');
    var date = new Date();
    date.setDate(date.getDate() + 1);

    $.post(app.config.server + app.config.calendarPath, {
        mobile: true,
        GetAllScrums: true,
        Start: createDate(false),
        End: createDate(date)
    }).done(function (data) {
        $('#itScrumsPage').show().html(data).find('.spinner').remove();
    });
}

function pushRoomsPage (event) {
    'use strict';

    pushSecondaryPage(event, 'roomsPage', false);

    startLoading('roomsPage');
    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Command: 'GetRoomsList'
    }).done(function (data) {
        /* global spinner */
        spinner.stop();
        $('.rooms-list').html(data);

        // Bind list items to show multi-location calendar
        $('.btn-location').on('touchend', function (e) {
            var item = $(this);
            var classes = item.prop('class');
            var nextColor = $(colors).not(app.usedColors).get();

            if (classes.indexOf('color') === -1) { // no color class
                item.addClass(nextColor[0]).addClass('selected');
                app.usedColors.push(nextColor[0]);
            } else { // remove color class
                $.each(app.usedColors, function (key, value) {
                    if (classes.indexOf(value) !== -1) { // found color
                        item.removeClass(value).removeClass('selected');
                        app.usedColors.splice(app.usedColors.indexOf(value), 1);
                    }
                });
            }

            loadMultiCalendar(this);
        });

        // Bind on click to add meeting buttons
        $('.btn-add-meeting').on('touchend', function (e) {
            loadMeetingModal(this);
            e.stopPropagation(); // keep it from selecting the list item
        });

        // Bind on click to meeting time buttons
        $('.btn-add-time, .btn-subtract-time').on('touchend', function (e) {
            changeMeetingTime(this);
        });

        // Bind on click to meeting time buttons
        $('.btn-add-meeting-accept').on('touchend', function (e) {
            addMeeting(this);
        });
    });
}


