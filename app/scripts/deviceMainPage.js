$(document).ready(function () {
    'use strict';

    var deviceListPopover = $('#deviceListPopover');

     // Tabbar Buttons
    $('#btnPhonePage').on('touchend', showPhonePage);
    $('#btnCameraPage').on('touchend', showCameraPage);
    $('#btnToolsPage').on('touchend', showToolsPage);

    // Tabbar buttons highlight
    $('.tabbar-item').on('touchend', function (e) {
        e.stopImmediatePropagation();
        var item = $(e.currentTarget);
        // Remove active from previous item
        $('.navbar-tabbar .nav li.active').removeClass('active');
        // Add active class to target item
        item.addClass('active');
    });

    // Device main page buttons
    $('#btnLayouts').on('touchend', showPresentationLayouts);
    $('.btn-layouts').on('touchend', changePresentationLayout);
    $('.btn-mic').on('touchend', toggleMicrophone);
    $('.btn-presentation').on('touchend', togglePresentation);
    $('.btn-selfview').on('touchend', toggleSelfView);
    $('body').on('touchend', '.device-list-group', selectDevice);
    $('#btnCalendar').on('touchend', showCalendarPage);
    $('#currentMeetingPhoneNumber').on('touchend', 'a', dialScrumNumber);

    // Initialize volume slider
    app.volumeSlider = $('.volume-slider').slider().on('slideStop',function () {
        onChangeVolume(100 - app.volumeSlider.getValue());
    }).data('slider');

    // Initialize device list popover
    deviceListPopover.popover({
        html: true,
        placement: 'bottom',
        title: 'Devices',
        trigger: 'manual',
        content: function () {
            loadDeviceList();
            return 'Loading...';
        }
    }).on('touchend', toggleDeviceListPopover).on('shown.bs.popover', function (e) {
        // Position on the middle of the header bar and make it responsive
        deviceListPopover.siblings('.popover').addClass('popover-center');

        $('#deviceListPopover .caret').addClass('caret-opened');
        deviceListPopover.css({'color': '#999'});
        if (app.device.current) {
            // Show Connected device
            var listItems = $(this).parent().find('.popover .list-group-item');
            $(listItems).filter(function () {
                return ($(this).text()).trim() === app.device.current.description;
            }).addClass('active');
        }
    }).on('hidden.bs.popover', function () {
        $('#deviceListPopover .caret').removeClass('caret-opened');
        deviceListPopover.css({'color': '#FFF'});
    });

    // Hide device list when clicking outside
    $('body').on('touchend', function (e) {
        var popover = $('.popover');
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (popover && !popover.is(e.target) &&
            popover.has(e.target).length === 0 &&
            popover.has(e.target).length === 0) {
            deviceListPopover.popover('hide');
            popover.hide(); // For some reason the previous hide command is not working "completely"
        }
    });
});

function updateSync () {
    if (app.device.current) {
        // Update status sync
        app.deviceStatusSync.postUrl = app.config.server + app.config.ciscoPath;
        app.deviceStatusSync.data = {
            Command: 'GetStatus',
            deviceIP: app.device.current.ip_address
        };
        if (!app.deviceStatusSync.run) {
            app.deviceStatusSync.start();
        }

        // Update notifications sync
        app.notificationsSync.postUrl = app.config.server + app.config.notificationPath;
        app.notificationsSync.data = {
            ID: 0,
            User: (app.showSelfNotifications) ? "" : app.currentUser,
            DeviceIP: app.device.current.ip_address
        }
        if (!app.notificationsSync.run) {
            app.notificationsSync.start();
        }

        // Update snapshot sync
        app.snapshotSync.postUrl = app.config.server + app.config.ciscoPath;
        app.snapshotSync.data = {
            mobile: true,
            Username: app.currentUser,
            Command: 'UpdateSnapshot',
            deviceIP: app.device.current.ip_address
        };
    }
}

function startMeetingTimer () {
    'use strict';

    if (!app.device.current) {
        return false;
    }

    var nextMeetingStartUnixTime = 0,
        room = app.rooms[app.device.current.description];

    if (!room) {
        $('#alertMeeting').hide();
        return false;
    }

    $.post(app.config.server + app.config.calendarPath, {
        GetCurrentNextMeeting: true,
        Command: 'GetCurrentNextMeeting',
        FromUser: room.room_id
    }).done(function (data) {
        /* jshint bitwise: false */
        /* global countdown */

        if (!data) { // No meeting data.
            return false;
        }

        var meeting = $.parseJSON(data),
            alertMeeting = $('.alert-meeting'),
            currentMeeting = $('#currentMeeting'),
            currentMeetingTitle = $('#currentMeetingTitle'),
            currentMeetingTimer = $('#currentMeetingTimer'),
            currentMeetingStatus = $('#currentMeetingStatus'),
            currentMeetingPhoneNumber = $('#currentMeetingPhoneNumber'),
            nextMeetingTitle = $('#nextMeetingTitle'),
            nextMeetingTime = $('#nextMeetingTime');

        // Do nothing if no next meeting title
        if (!meeting.nextMeetingTitle) {
            alertMeeting.hide();
            return false;
        } else {
            // Set next meeting data
            nextMeetingTitle.html(meeting.nextMeetingTitle);
            nextMeetingTime.html(meeting.nextMeetingTime);

            if (!meeting.currentMeetingTitle) {
                currentMeeting.hide();
                alertMeeting.removeClass('alert-warning alert-danger');
                alertMeeting.addClass('alert-info');

                // Clear current meeting
                currentMeetingTitle.html('');

                // Stop timer
                window.clearInterval(app.timerId);
            } else if (meeting.currentMeetingTitle !== currentMeetingTitle.text()) { // start a new timer
                // Set current meeting data
                currentMeetingTitle.html(meeting.currentMeetingTitle);
                currentMeetingPhoneNumber.append($('<a>', {'href': '#', 'text': meeting.currentMeetingPhoneNumber}));
                if (!meeting.currentMeetingPhoneNumber) {
                    currentMeetingPhoneNumber.hide();
                }
                currentMeeting.show();

                // Create counter
                var date = new Date(meeting.currentMeetingUnixTime * 1000);
                app.timerId = countdown(date, function (ts) {
                    if (ts.minutes == 0 && ts.seconds == 0) {
                        // Meeting ended
                        currentMeeting.hide();
                        window.clearInterval(app.timerId);
                    }
                    if (ts.minutes < 5 && ts.hours == 0 ) {
                        // Show red alert when timer is under 5 mins.
                        alertMeeting.removeClass('alert-info alert-warning');
                        alertMeeting.addClass('alert-danger');
                    } else if (ts.minutes < 10 && ts.hours == 0) {
                        // Show yellow alert when timer is under 10 mins
                        alertMeeting.removeClass('alert-info alert-danger');
                        alertMeeting.addClass('alert-warning');
                    } else {
                        alertMeeting.removeClass('alert-warning alert-danger');
                        alertMeeting.addClass('alert-info');
                    }

                    var timerHTML = ('0' + ts.hours).slice(-2) + ':' + ('0' + ts.minutes).slice(-2) + ':' + ('0' + ts.seconds).slice(-2);
                    currentMeetingTimer.html(timerHTML);

                }, countdown.HOURS | countdown.MINUTES | countdown.SECONDS);
            }
            alertMeeting.show();
        }
    });
}

function syncDevice (data) {
    'use strict';

    // Call meeting timer to check for new meetings
    startMeetingTimer();

    // Do nothing if no device selected
    if (!app.device.current) {
        // Disable device buttons
        $('#cameraPage button').prop('disabled', true);
        return false;
    }

    // Enable device buttons
    $('#cameraPage button').prop('disabled', false);

    // Increment app.syncCalls var. Used to only take into account the latest
    // call to this function. Usually triggered by a command instead of byt the
    // timer. This ensures that we only get the latest data after a command has
    // changed any remote configuration.
    app.syncCalls++;

    if (app.syncCalls > 1) {
        app.syncCalls--;
        return false;
    }
    app.syncCalls--;

    if (!data) {
        return false;
    }

    app.ajaxResult = $.parseJSON(data);

    // Update volume controls
    app.device.micStatus = app.ajaxResult.Audio.Microphones.Mute;
    updateMicrophone();

    // Update call status
    app.device.inCall = (typeof app.ajaxResult.Call === 'undefined') ? false : true;
    $('.btn-next-video-layout').prop('disabled', !app.device.inCall);
    updateCallButton();

    // Set volume
    app.volumeSlider.setValue(100 - app.ajaxResult.Audio.Volume);

    // Set camera
    app.camera.position.pan  = app.ajaxResult.Camera.Position.Pan;
    app.camera.position.tilt = app.ajaxResult.Camera.Position.Tilt;
    app.camera.position.zoom = app.ajaxResult.Camera.Position.Zoom;
    app.camera.connected     = app.ajaxResult.Camera.Connected;
    app.camera.model         = app.ajaxResult.Camera.Model;
    app.camera.selfviewOn    = (app.ajaxResult.Video.Selfview.Mode === 'On') ? true : false;
    updateSelfview();

    // Update phone page
    if (app.device.inCall && $('#phonePage').css('visibility') !== 'hidden') {
        if (app.ajaxResult.Call instanceof Array) {
            for (var i in app.ajaxResult.Call) {
                app.device.dialedNumber = app.ajaxResult.Call[i].RemoteNumber.replace(app.device.MCU_IP, '').replace(app.config.VCS_ADDRESS, '');
                $('#dialPad').hide();
                updateInCallList();
                $('#dialInCall').show();
            }
        } else {
            app.device.dialedNumber = app.ajaxResult.Call.RemoteNumber.replace(app.config.MCU_IP, '').replace(app.config.VCS_ADDRESS, '');
            $('#dialPad').hide();
            updateInCallList();
            $('#dialInCall').show();
        }
    } else {
        $('#dialInCall').hide();
        $('#dialPad').show();
    }

    // Update presentation
    app.device.presentationOn = (app.ajaxResult.Conference.Presentation.Mode === 'Sending') ? true : false;
    $('#btnLayouts').toggle(app.device.presentationOn); // Hide or show button
    updatePresentation();

    // Update Layout
    var output = $(app.ajaxResult.Video.Layout.Site).first().prop('Output');
    app.device.presentationLayout = $(output).first().prop('FamilyName');
    $('.btn-number').first();
    highlightCurrentLayout();
}

function refreshData () {
    'use strict';

    if (!app.device.current) {
        bootbox.alert("Please select a device");
        return false;
    }

    var spinElement = $('.device-sync i');

    spinElement.addClass('spin');
    window.setTimeout(function () {spinElement.removeClass('spin');}, 4000);
}

function noDeviceSelectedAction () {
    'use strict';
    bootbox.alert('Please select a device first');
}

function toggleDeviceListPopover (e) {
    'use strict';
    $('#deviceListPopover').popover('toggle');
    e.stopPropagation();
}

function toggleMicrophone () {
    'use strict';

    var params = {};

    if (app.device.micStatus === 'Off') {
        app.device.micStatus = 'On';

        params = {
            Username: app.currentUser,
            Command: 'Mute',
            deviceIP: app.device.current.ip_address
        };
    } else {
        app.device.micStatus = 'Off';

        params = {
            Username: app.currentUser,
            Command: 'Unmute',
            deviceIP: app.device.current.ip_address
        };
    }

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    $.post(app.config.server + app.config.ciscoPath, params).done(function () {
        app.syncCalls = 0;
        syncDevice();
    });
    updateMicrophone();
}

function updateMicrophone () {
    'use strict';

    var button = $('.btn-mic');

    if (app.device.micStatus === 'Off') {
        button.removeClass('icon-microphone-mute');
        button.addClass('icon-microphone');
    } else {
        button.addClass('icon-microphone-mute');
        button.removeClass('icon-microphone');
    }
}

function toggleSelfView () {
    'use strict';

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    if (app.camera.selfviewOn) {
        app.camera.selfviewOn = false;

        $.post(app.config.server + app.config.ciscoPath, {
            Username: app.currentUser,
            Command: 'HideSelfview',
            deviceIP: app.device.current.ip_address
        }).done(function (data) {
            app.ajaxResult = data;
            app.syncCalls = 0;
            syncDevice();
        });
    } else {
        app.camera.selfviewOn = true;

        $.post(app.config.server + app.config.ciscoPath, {
            Username: app.currentUser,
            Command: 'ShowSelfview',
            deviceIP: app.device.current.ip_address
        }).done(function (data) {
            app.ajaxResult = data;
            app.syncCalls = 0;
            syncDevice();
        });
    }

    updateSelfview();
}

function updateSelfview () {
    'use strict';
    var button = $('.btn-selfview'),
        pip = $('.selfview');

    if (app.camera.selfviewOn) {
        button.children().css({
            color: '#004513',
            border: '2px solid #004513'
        });
        pip.show();
    } else {
        button.children().css({
            color: '#999',
            border: '1px solid #999'
        });
        pip.hide();
    }
}

function togglePresentation () {
    'use strict';

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    if (!app.device.presentationOn) {
        app.device.presentationOn = true;

        $.post(app.config.server + app.config.ciscoPath, {
            Username: app.currentUser,
            Command: 'StartPresentation',
            PresentationSource: 2,
            deviceIP: app.device.current.ip_address
        }).done(function (data) {
            app.ajaxResult = data;
            app.syncCalls = 0;
            syncDevice();
        });
    } else {
        app.device.presentationOn = false;

        $.post(app.config.server + app.config.ciscoPath, {
            Username: app.currentUser,
            Command: 'StopPresentation',
            PresentationSource: 2,
            deviceIP: app.device.current.ip_address
        }).done(function (data) {
            app.ajaxResult = data;
            app.syncCalls = 0;
            syncDevice();
        });
    }

    $('#btnLayouts').toggle();
    updatePresentation();
}

function updatePresentation () {
    'use strict';

    var button = $('.btn-presentation'),
        layout = $('.presentation-layout');

    if (app.device.presentationOn) {
        button.css({'background-color': '#004513'});
        button.find('.btn-presentation-lbl').text('Stop Presenting');
        updateLayout();
        layout.slideDown();
    } else {
        button.css({'background-color': '#999'});
        button.find('.btn-presentation-lbl').text('Present');
        layout.slideUp();
    }

    updateLayout();
}

function updateLayout () {
    'use strict';

    var layout = app.device.presentationLayout,
        layoutDiv = $('.presentation-layout'),
        layoutUser = $('.layout-user'),
        layoutLaptop = $('.layout-laptop');

    layoutLaptop.show();
    switch (layout) {
        case 'equal':
            layoutUser.removeClass(); // empty class
            layoutUser.addClass('layout-user layout-equal');
            layoutLaptop.removeClass(); // empty class
            layoutLaptop.addClass('layout-laptop layout-equal');
            break;
        case 'prominent':
            layoutUser.removeClass(); // empty class
            layoutUser.addClass('layout-user layout-prominent');
            layoutLaptop.removeClass(); // empty class
            layoutLaptop.addClass('layout-laptop layout-prominent');
            break;
        case 'overlay':
            layoutUser.removeClass(); // empty class
            layoutUser.addClass('layout-user layout-overlay');
            layoutLaptop.removeClass(); // empty class
            layoutLaptop.addClass('layout-laptop layout-overlay');
            break;
        case 'single':
            layoutUser.removeClass(); // empty class
            layoutUser.addClass('layout-user layout-single');
            layoutLaptop.removeClass(); // empty class
            layoutLaptop.addClass('layout-laptop layout-single');
            layoutLaptop.hide();
            break;
    }

    // Resize layout elements
    resizeElements();
}

function onClickNextVideoLayout () {
    'use strict';

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    $.post(app.config.server + app.config.ciscoPath, {
        Username: app.currentUser,
        Command: 'NextRemoteLayout',
        callId: app.device.callId,
        deviceIP: app.device.current.ip_address
    }).done(function () {
        app.syncCalls = 0;
        syncDevice();
    });
}

function showPresentationLayouts () {
    'use strict';

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    $('#presentationModalTitle').html('Presentation Layouts');
    $('#presentationModal').modal('show').on('hidden.bs.modal', function () {
        app.syncCalls = 0;
        syncDevice();
    });
}

function highlightCurrentLayout () {
    'use strict';

    var presentationLayoutBtnCaption = $('.presentation-layout-button-caption');

    presentationLayoutBtnCaption.siblings('button').removeClass('active');
    presentationLayoutBtnCaption.each(function () {
        if (app.device.presentationLayout.toLowerCase() === $(this).text().toLowerCase()) {
            $(this).siblings('button').addClass('active');
        }
    });
}

function changePresentationLayout () {
    'use strict';

    var layout = $(this).find('div').prop('class').replace('icon-layout-', '');
    layout = (layout === 'icon-user') ? 'single' : layout;

    app.device.presentationLayout = layout;
    highlightCurrentLayout();
    updateLayout();

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    // There is a bug on the cisco API that prevents layout changes from
    // without coming from the prominent layout first. So here we are forcing
    // it to go to prominent layout first before sending any other layout change.
    if (layout !== 'prominent') {
        $.post(app.config.server + app.config.ciscoPath, {
            Username: app.currentUser,
            Command: 'ChangeLayout',
            layout: 'prominent',
            deviceIP: app.device.current.ip_address
        }).done(function (data) {
            $.post(app.config.server + app.config.ciscoPath, {
                Username: app.currentUser,
                Command: 'ChangeLayout',
                layout: layout,
                deviceIP: app.device.current.ip_address
            }).done(function () {
                app.syncCalls = 0;
                syncDevice();
            });
        });
    } else {
        $.post(app.config.server + app.config.ciscoPath, {
            Username: app.currentUser,
            Command: 'ChangeLayout',
            layout: layout,
            deviceIP: app.device.current.ip_address
        }).done(function () {
            app.syncCalls = 0;
            syncDevice();
        });
    }
}

function onChangeVolume (volume) {
    'use strict';

    if (!app.device.current) {
        noDeviceSelectedAction();
        return false;
    }

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    $.post(app.config.server + app.config.ciscoPath, {
        Username: app.currentUser,
        Command: 'ChangeVolume',
        volume: volume,
        deviceIP: app.device.current.ip_address

    }).done(function () {
        app.syncCalls = 0;
        syncDevice();
    });
}

function showDeviceMainPage (e) {
    'use strict';

    // Modify header, hide home label, show home button and hide logout button
    $('.home-label').hide();
    $('#btnHomePage').show();
    $('#btnLogout').hide();
    $('#deviceListPopover').show();

    // Stop snapshot updater
    if (app.snapshotSync.run) {
        app.snapshotSync.stop();
    }

    if (!app.device.current) {
        $('#deviceListPopover').popover('show');
        $('.tabbar-item').not('.active').addClass('tabbar-item-inactive');
        (e) ? e.stopPropagation() : false ;
    }

    resizeElements();
}

function loadDeviceList () {
    'use strict';

    var deviceListPopover = $('#deviceListPopover');

    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Command: 'GetDeviceList'
    }).done(function (data) {
        deviceListPopover.attr('data-content', data);
        var popover = deviceListPopover.data('bs.popover');
        popover.$tip.find('.popover-content')[popover.options.html ? 'html' : 'text'](data);
        popover.$tip.addClass(popover.options.placement);
    });
}

function selectDevice (e) {
    'use strict';

    var selectedDevice = e.target,
        newDevice = $(selectedDevice).data('device-data'),
        deviceListPopover = $('#deviceListPopover');

    selectedDevice = $(selectedDevice);

    if (!selectedDevice.hasClass('active') && newDevice) {  // Connect
        app.device.current = newDevice;
        deviceListPopover.html(app.device.current.description + ' <span class="caret caret-opened"></span>');
        selectedDevice.siblings().removeClass('active');
        $('.tabbar-item-inactive').removeClass('tabbar-item-inactive');
        updateSync();
        loadCameraPresets();
    } else { // Disconnect
        app.device.current = undefined;
        deviceListPopover.html('Select Device <span class="caret caret-opened"></span>');
        app.snapshotSync.stop();
        app.deviceStatusSync.stop();
        app.notificationsSync.stop();

        // Update dialpad
        $('#dialPad').show();
        $('#dialInCall').hide();
        app.device.inCall = false;
        updateCallButton();

        // Update tabbar
        $('.btn-device-main-page').trigger('touchend');
        $('.tabbar-item').not('.active').addClass('tabbar-item-inactive');

        // Hide meeting alert
        $('.alert-meeting').hide();
    }

    selectedDevice.toggleClass('active');
    window.clearInterval(app.timerId);
    startMeetingTimer();
    syncDevice();
    deviceListPopover.popover('toggle');

    // Replace snapshot with placeholder and call API to get a new from the new
    // device
    $('#cameraPreview img').attr('src', 'images/voyager.png');
}

function showPhonePage (e) {
    'use strict';

    if (!app.device.current) {
        bootbox.alert("Please select a device", function () {
            setTimeout(function () {
                $('.btn-device-main-page').trigger('touchend')
            }, 200);
        });
        return;
    }

    showDeviceMainPage();
    syncDevice();
    (e) ? e.stopPropagation() : false ;
}

function showCameraPage (e) {
    'use strict';

    if (!app.device.current) {
        bootbox.alert("Please select a device", function () {
            setTimeout(function () {
                $('.btn-device-main-page').trigger('touchend')
            }, 200);
        });
        return;
    }

    showDeviceMainPage();
    $('#cameraPage').show();
    syncDevice();
    (e) ? e.stopPropagation() : false ;

    // Start snapshot updater
    if (!app.snapshotSync.run) {
        app.snapshotSync.start();
    }

    // Load device presets
    loadCameraPresets();
}

function showToolsPage (e) {
    'use strict';

    if (!app.device.current) {
        bootbox.alert("Please select a device", function () {
            setTimeout(function () {
                $('.btn-device-main-page').trigger('touchend')
            }, 200);
        });
        return;
    }

    showDeviceMainPage();
    $('#toolsPage').show();
    syncDevice();
    (e) ? e.stopPropagation() : false ;
}

function autoSync () {
    'use strict';

    setTimeout(function () {
        syncDevice();
        autoSync();
    }, (3 * 1000));
}

function showAlerts (data) {
    'use strict';

    if (!data || !app.device.current) { // Ajax error
        return false;
    }

    var json = $.parseJSON(data);

    // Do not show notification if older than a minute
    if (new Date().getTime() - new Date(json.EVENT_TIME).getTime() > 60000) {
        return false;
    }

    app.notificationsSync.data = {
        ID: json.ID,
        DeviceIP: app.device.current.ip_address,
        User: (app.showSelfNotifications) ? "" : app.currentUser
    }

    $('#alertBody').html(json.EVENT + ' - ' + json.EVENT_TIME);
    $('#alertBody').slideDown(200).delay(2000).slideUp(200);
}

