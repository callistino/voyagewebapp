/**
 * @author Reinier Pelayo
 *
 * This class builds a list of MCU participants with a timer showing how long they have
 *
 * been connected and buttons to end/mute each participant. 
 * @depencies [jQuery, Counter.js, Device.js]
 *
 */
MCUParticipantList = (function () {
    __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

	/**
	 * Constructor
	 * 
	 * @param DOM element element 	   The DOM element parent of the list
     * @param Object      data         Object with data to continue fetching
	 */
    function MCUParticipantList (options) {
    	// Properties
        this.element            = options.element;
        this.allParticipants    = options.allParticipants;
        this.callParticipants   = options.callParticipants;
        this.participants       = {};
        this.callTimers         = {};
        this.updateTimeout      = {};
        this.buttonEndIdSuffix  = 'end';
        this.buttonMuteIdSuffix = 'mute';
        this.timerIdSuffix      = 'timer';
        this.participantsCount  = 0;

        // Methods
        this.buildList                      = __bind(this.buildList, this);
        this.fetchMCUParticipants           = __bind(this.fetchMCUParticipants, this);
        this.fetchCallParticipants          = __bind(this.fetchCallParticipants, this);
        this.conferences                    = __bind(this.conferences, this);
        this.findPanelInConference          = __bind(this.findPanelInConference, this);
        this.updateConferenceDataOnPanel    = __bind(this.updateConferenceDataOnPanel, this);
        this.updateConferenceMuteStatus     = __bind(this.updateConferenceMuteStatus, this);
        this.updateConferenceMuteButton     = __bind(this.updateConferenceMuteButton, this);
        this.updateParticipantMuteButton    = __bind(this.updateParticipantMuteButton, this);
        this.updateListItemChildrenIds      = __bind(this.updateListItemChildrenIds, this);
        this.removeEmptyConferences         = __bind(this.removeEmptyConferences, this);
        this.removeDisconnectedParticipants = __bind(this.removeDisconnectedParticipants, this);
        this.toggleMuteAllMCUCalls          = __bind(this.toggleMuteAllMCUCalls, this);
        this.toggleMuteMCUCall              = __bind(this.toggleMuteMCUCall, this);
        this.disconnectMCUCall              = __bind(this.disconnectMCUCall, this);
        this.allParticipantsMuted           = __bind(this.allParticipantsMuted, this);

        // Events
	    this.element.on('touchend', '.btn-mute-all-participants', this.toggleMuteAllMCUCalls);
	    this.element.on('touchend', '.btn-mute-MCU-call', this.toggleMuteMCUCall);
	    this.element.on('touchend', '.btn-end-MCU-call', this.disconnectMCUCall);

        // Init
        if (this.allParticipants) {
            this.fetchMCUParticipants();
        } else {
            this.fetchCallParticipants();
        }
    }

    MCUParticipantList.prototype.buildList = function() {
    	if ($.isEmptyObject(this.participants)) {
    		return this.list;
    	}

    	for (var conference in this.conferences()) {
    		var panel = this.findPanelInConference(conference) || panelStub();

            for (var i in this.conferences()[conference]) {
                var participant = this.conferences()[conference][i],
                    listItemId  = participant.participantName,
                    listItem    = this.findListItemInPanel(listItemId, panel) || listItemStub();
                if (!this.callTimers[listItemId + this.timerIdSuffix]) { // Add new participant
                    listItem.prop('id', listItemId).data('participant', participant);
                    this.updateListItemChildrenIds(listItem, listItemId);
                    updateListItemLabel(listItem, participant);
                    panel.find('.list-group').append(listItem);
                    this.callTimers[listItemId + this.timerIdSuffix] = createParticipantTimer(participant, listItem.find('.list-group-item-timer'));
                }

                this.updateParticipantMuteButton(panel, participant);
            }

    		updateConferenceNameOnPanel(panel, conference);
    		this.updateConferenceDataOnPanel(panel, conference);
            this.updateConferenceMuteStatus(panel, conference);

	        // Remove all participants and conferences
	        this.removeDisconnectedParticipants();
	        this.removeEmptyConferences();
	        
	        // Update data
	        this.element.append(panel);
    	}
    };

    MCUParticipantList.prototype.fetchMCUParticipants = function() {
        // Bind this since it changes inside .done
        var self = this;

        $.post(app.config.server + app.config.mcuPath, {
            mobile:   true,
            Username: app.currentUser,
            Command:  'GetMCUParticipants',
        }).done(function (data) {
            if (data && $.parseJSON(data)) {
                self.participants = $.parseJSON(data);
                window.clearTimeout(self.updateTimeout);
                self.updateTimeout = setTimeout(self.fetchMCUParticipants, 5000);
                self.buildList();
            }
            self.element.trigger('doneMCURequest');
        });
    };

    MCUParticipantList.prototype.fetchCallParticipants = function() {
        // Bind this since it changes inside .done
        var self = this;

        $.post(app.config.server + app.config.mcuPath, {
            mobile:       true,
            Username:     app.currentUser,
            Command:      'GetMCUCallList',
            deviceIP:     app.device.current.ip_address,
            dialedNumber: app.device.dialedNumber
        }).done(function (data) {
            if (data && $.parseJSON(data)) {
                self.participants = $.parseJSON(data).participants;
                self.participantsCount = $.parseJSON(data).globalConnections;
                window.clearTimeout(self.updateTimeout);
                self.updateTimeout = setTimeout(self.fetchCallParticipants, 5000);
                self.buildList();
            }
        });
    };

    MCUParticipantList.prototype.conferences = function() {
    	var conferences = {};

    	this.participants.map(function (participant) {
    		if (!conferences[participant.conferenceName]){
    			conferences[participant.conferenceName] = [];
    		}
    		conferences[participant.conferenceName].push(participant);
    	});

    	return conferences;
    };
    
    MCUParticipantList.prototype.findPanelInConference = function (conference) {
    	var panel = this.element.find('.conference-name:contains(' + conference + ')').parent().parent();
    	return (panel.length > 0) ? panel : false;
    };

    MCUParticipantList.prototype.findListItemInPanel = function (id, panel) {
        var listItem = panel.find('#' + id);
        return (listItem.length > 0) ? listItem : false;
    };

    MCUParticipantList.prototype.updateConferenceDataOnPanel = function (panel, conference) {
    	panel.find('.panel-heading').data('participants', this.conferences()[conference]);
    };

    MCUParticipantList.prototype.updateConferenceMuteStatus = function (panel, conference) {
        var participants = this.conferences()[conference],
            button       = panel.find('.panel-heading button');

        this.updateConferenceMuteButton(button, participants);
    };

    MCUParticipantList.prototype.updateConferenceMuteButton = function(button, participants) {
        if (this.allParticipantsMuted(participants)) {
            button.children('i').removeClass('icon-microphone').addClass('icon-microphone-mute');
            button.html(button.html().replace(button.text(),'Unmute All'));
        } else {
            button.children('i').removeClass('icon-microphone-mute').addClass('icon-microphone');
            button.html(button.html().replace(button.text(),'Mute All'));
        }
    };

    MCUParticipantList.prototype.updateParticipantMuteButton = function (panel, participant) {
        var button = panel.find('#' + participant.participantName + this.buttonMuteIdSuffix);

        if (participant.audioRxMuted === '0') {
            button.removeClass('icon-microphone-mute').addClass('icon-microphone');
        } else {
            button.addClass('icon-microphone-mute').removeClass('icon-microphone');
        }
    }

    MCUParticipantList.prototype.updateListItemChildrenIds = function (listItem, id) {
    	listItem.find('.btn-mute-MCU-call').prop('id', id + this.buttonMuteIdSuffix);
    	listItem.find('.btn-end-MCU-call').prop('id', id + this.buttonEndIdSuffix);
    	listItem.find('.list-group-item-timer').prop('id', id + this.timerIdSuffix);
    };

    MCUParticipantList.prototype.allParticipantsMuted = function (participants) {
        var callers = participants || this.participants;
        for (var index in callers) {
            if (callers[index].audioRxMuted === '0') {
                return false;
            }
        }

        return true;
    };

    MCUParticipantList.prototype.removeDisconnectedParticipants = function() {
    	for (var timerId in this.callTimers) {
            var displayName = '';

            for (var k in this.participants) {
                if (this.participants[k].participantName + this.timerIdSuffix === timerId) {
                    displayName = this.participants[k].displayName;
                }
            }

            if (!displayName) { // Remove list entry
                this.element.find('#' + timerId).parent().remove();
                this.callTimers[timerId].stop();
                delete this.callTimers[timerId];
            }
        }
    };

    MCUParticipantList.prototype.removeEmptyConferences = function() {
    	var activeConf = Object.keys(this.conferences());
        this.element.find('.conference-name').each(function () { 
        	if (activeConf.indexOf($(this).text()) < 0) {
        		$(this).parents('.panel').remove();
        	}
        });
    };

    MCUParticipantList.prototype.toggleMuteAllMCUCalls = function (event) {
        'use strict';

        event.stopImmediatePropagation();

        // Bind this since it changes inside .done
        var self = this,
            button = $(event.target),
            mute = '1',
            participants = button.parent().data('participants'); // Only this conference participants

        this.updateConferenceMuteButton(button, participants);

        if (self.allParticipantsMuted(participants)) {
            mute = '0';
        }

        for (var i in participants) {
            participants[i].audioRxMuted = mute;
        }

        $.post(app.config.server + app.config.mcuPath, {
            mobile:          true,
            Username:        app.currentUser,
            Command:         'ModifyAllParticipantsMute',
            participantData: participants,
        }).done(function (data) {
            if (self.allParticipants) {
                self.fetchMCUParticipants();
            } else {
                self.fetchCallParticipants();
            }
        });
    };
    
    MCUParticipantList.prototype.disconnectMCUCall = function (event) {
        'use strict';

        event.stopImmediatePropagation();

        // Bind this since it changes inside .done
        var self = this,
            button = $(event.target),
            participant = button.parent().data('participant');

        // Set to the font color of the button to the same as the 
        // background to hide the icon
        button.css({'color': button.css('background-color')});
        startLoading(button.prop('id'), true);

        $.post(app.config.server + app.config.mcuPath, {
            mobile:          true,
            Username:        app.currentUser,
            Command:         'DisconnectParticipant',
            participantData: participant,
        }).done(function (data) {
            if (self.allParticipants) {
                self.fetchMCUParticipants();
            } else {
                self.fetchCallParticipants();
            }
        });
    };

    MCUParticipantList.prototype.toggleMuteMCUCall = function (event) {
        'use strict';

        event.stopImmediatePropagation();

        // Bind this since it changes inside .done
        var self = this,
            button = $(event.target),
            muteAllBtn = button.parents('.panel').find('.panel-heading button'),
            participants = muteAllBtn.parent().data('participants'), // Only this conference participants
            participant = button.parent().data('participant');
            
        participant.audioRxMuted = participant.audioRxMuted === '0' ? '1' : '0';

        // Set to the font color of the button to the same as the 
        // background to hide the icon
        button.css({'color': button.css('background-color')});
        startLoading(button.prop('id'), true);

        $.post(app.config.server + app.config.mcuPath, {
            mobile:          true,
            Username:        app.currentUser,
            Command:         'ModifyParticipantMute',
            participantData: participant,
        }).done(function (data) {
            if (self.allParticipants) {
                self.fetchMCUParticipants();
            } else {
                self.fetchCallParticipants();
            }

            button.find('.spinner').remove();
            button.css({'color': 'initial'});
        });
    };

    function panelStub () {
        var list         = $('<ul class="list-group">'),
        	panelHeading = $('<div class="panel-heading">'+
	        					'<b class="conference-name"></b>'+
	        					'<button class="btn-mute-all-participants btn pull-right" type="button">'+
	        						'<i class="icon-microphone"></i>Mute All'+
	    						'</button>'+
						    '</div>'),
        	panel		 = $('<div class="panel panel-default">').append(panelHeading, list);

        return panel;
    };

    function listItemStub () {
    	var button       = $('<button class="btn list-group-item-button pull-right">'),
        	buttonEnd    = button.clone().addClass('btn-end-MCU-call icon-phone-hangup'),
        	buttonMute   = button.clone().addClass('btn-mute-MCU-call icon-microphone'),
            listLabel    = $('<div class="list-group-item-label">'),
            timerDiv	 = $('<div class="list-group-item-timer">'),
        	listItem     = $('<li class="list-group-item">').append(listLabel, buttonMute, buttonEnd, timerDiv);

		return listItem;
    }

    function updateListItemLabel (listItem, participant) {
    	listItem.find('.list-group-item-label').text(participant.displayName);
    }

	function updateConferenceNameOnPanel (panel, conference) {
    	panel.find('.conference-name').text(conference);
    }

    function updateParticipantOnList (list, participant) {
    	var listId = participant.participantName; // Unique participant IDlistLabel
        list.prop('id', listId);
	}

    function createParticipantTimer (participant, timerDiv) {
    	var connectTime     = [participant.connectTime.slice(0, 4), '-', participant.connectTime.slice(4, 6), '-', participant.connectTime.slice(6)].join(''),
            connectDateTime = new Date(connectTime).getTime(),
            todayDateTime   = new Date().getTime(),
            timeDiff        = new Date(todayDateTime - connectDateTime).getTime();

    	var timer = new Counter(timeDiff, 1000, 1000, function () {
	        var timerDate = new Date(this.counter);
	        var timerText = ('0' + timerDate.getHours()).slice(-2) + ':' +
	                        ('0' + timerDate.getMinutes()).slice(-2) + ':' +
	                        ('0' + timerDate.getSeconds()).slice(-2);
	        timerDiv.html(timerText);
	    });

	    timer.start();
    	return timer;
	}

    return MCUParticipantList;

})();

