/**
 * @author Reinier Pelayo
 *
 * @param element The DOM element representing the view controller
 *
 */
ViewController = (function () {
    __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    function ViewController (element) {
        this.view = element;
    }

    return ViewController;

})();

