var callList         = {},
    mcuList          = {},
    mcuActiveTimeout = '',
    callListTimeout  = '';

$(document).ready(function () {
    'use strict';

    var dialOutput = $('.dial-output p'),
        backspaceButton = $('.dial-output button');

    // Phone page buttons
    $('#btnMCUParticipants').on('touchend', showMCUParticipants);

    $('.btn-dial').on('touchend', function (e) {
        var button = $(e.currentTarget),
        buttonText = $.trim(button.text());

        if ((!isNaN(buttonText) || buttonText === '*' || buttonText === '#') && !button.is('.btn-call')) {
            backspaceButton.fadeIn();
            dialOutput.text(dialOutput.text() + buttonText);
        } else {
            switch (buttonText) {
                case 'Favorites':
                    showFavorites();
                    break;
                case 'Clear':
                    backspaceButton.fadeOut();
                    dialOutput.text('');
                    break;
            }
        }
    });

    $('.dial-output button').on('touchend', function (e) {
        // Delete last char
        dialOutput.text(dialOutput.text().substring(0, dialOutput.text().length - 1));

        // Check to see if the output is empty and hide backspace button
        if ($.trim(dialOutput.text()) === '') {
            dialOutput.text('');
            backspaceButton.fadeOut();
        }
    });

    $('.btn-call').on('touchend', function (e) {
        if (dialOutput.text() !== '' && !$(e.currentTarget).hasClass('btn-call-end')) {
            app.device.dialedNumber = dialOutput.text();
            dialNumber(app.device.dialedNumber);
            dialOutput.text('');
            app.device.inCall = true;
        } else {
            disconnectAllCalls();
            app.device.inCall = false;
            delete app.device.dialedNumber;
        }
        updateCallButton();
    });

    // Other inits
    $('.dial-output button').fadeOut();
});

function updateInCallList() {
    'use strict';

    var dialInCallList = $('#dialInCallList');

    if ($.isEmptyObject(callList)) {
        callList =  new MCUParticipantList({
            element: dialInCallList.find('.mcu-participants'), 
            callParticipants: true
        });
    }
    
    // Update heading
    if (!callList.participants) {
        dialInCallList.find('.conference-name').text('Not connected to an MCU conference');
    }

    // Update global connections count
    dialInCallList.find('p span').html(callList.participantsCount);
}

function updateCallButton () {
    'use strict';
    $('.btn-call').toggleClass('btn-call-end', app.device.inCall);
    $('.btn-call span').toggleClass('icon-phone', !app.device.inCall);
    $('.btn-call span').toggleClass('icon-phone-hangup', app.device.inCall);
}

function showMCUParticipants () {
    'use strict';

    if (!app.mcuActiveModal) {
        app.mcuActiveModal = bootbox.dialog({
            title: "MCU Active Participants",
            buttons: {},
            message: ' ', // Errors if blank
            className: 'modal-full-size'
        }).on('hidden.bs.modal', function (e) {
            // Destroy all timers
            for (var timerId in app.mcuActiveTimers) {
                app.mcuActiveTimers[timerId].stop();
                delete app.mcuActiveTimers[timerId];
            }
            app.mcuActiveModal = false;
            clearTimeout(mcuActiveTimeout);
        });
    }

    loadMCUParticipants();
}

function loadMCUParticipants () {
    'use strict';

    var element = app.mcuActiveModal.find('.bootbox-body');
        element.append($('<div class="mcu-participants">'));

    if ($.isEmptyObject(mcuList)) {
        mcuList =  new MCUParticipantList({
            element: element.find('.mcu-participants'), 
            allParticipants: true
        });
    }
}

function dialNumber (number) {
    'use strict';

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Username: app.currentUser,
        Command: 'Call',
        callNumber: number,
        deviceIP: app.device.current.ip_address
    }).done(function (data) {
        var result = $.parseJSON(data);
        app.device.callId = result.DialResult.CallId;
        $('.dial-output button').fadeOut();
        app.syncCalls = 0;
        syncDevice();
    });
}

function dialScrumNumber(event) {
    'use strict';

    var scrumNumber = $(event.currentTarget).text();
    bootbox.confirm('Do you want to dial this number: ' + scrumNumber + '?',
        function (result) {
            if (result) {
                dialNumber(scrumNumber);
            }
        });
}

function disconnectAllCalls () {
    'use strict';

    // We know this is the latest call to the Web API so we make all previous
    // syncDevice() calls invalid until we get a response for this specific
    // action
    app.syncCalls = 1000;

    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Username: app.currentUser,
        Command: 'DisconnectAll',
        deviceIP: app.device.current.ip_address
    }).done(function (data) {
        var result = $.parseJSON(data);
        app.syncCalls = 0;
        syncDevice();
    });
}

function showFavorites () {
    'use strict';

    loadFavorites();
    $('#favoritesModal').modal('show');
}

function loadFavorites () {
    'use strict';

    $.post(app.config.server + app.config.ciscoPath, {
        mobile: true,
        Command: 'GetFavorites',
        deviceIP: app.device.current.ip_address
    }).done(function (data) {
        $('#favoritesModal .modal-body').html(data);
    });
}

function onFavoriteClick (element) {
    'use strict';

    dialNumber($(element).find('.favorite-number').text());
    $('#favoritesModal').modal('hide');
}


