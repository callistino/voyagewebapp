/**
 * @author Reinier Pelayo
 *
 */
var App = (function () {
    function App () {
        this.config                = new Config(),
        this.currentUser           = '',
        this.prevPage              = 'homePage',
        this.currPage              = 'homePage',
        this.rAF                   = new AnimationFrame(),
        this.deviceStatusSync      = new AjaxUpdater(this.config.server + this.config.ciscoPath, {}, 3000, syncDevice),
        this.snapshotSync          = new AjaxUpdater(this.config.server + this.config.ciscoPath, {}, 3500, loadCameraPreview),
        this.notificationsSync     = new AjaxUpdater(this.config.server + this.config.notificationPath, {}, 0, showAlerts),
        this.device                = new Device(),
        this.camera                = new Camera(),
        this.mcuActiveTimers       = {},
        this.usedColors            = [],
        this.syncCalls             = 0,
        this.mcuActiveModal        = false,
        this.showSelfNotifications = false,
        this.ajaxResult            = '',
        this.volumeSlider          = '',
        this.rooms                 = '',
        this.timerId               = '',
        this.resizing              = false,
        this.latestWindowSize      = {height: 0, widht: 0};
        this.colors                = ['color1', 'color2', 'color3', 'color4', 'color5', 'color6',
                                      'color7', 'color8', 'color9', 'color10'];
    }

    return App;

})();

var app = new App();

// requestAnimationFrame polyfill (fAF) helps from calling the event handler
// on every tick. It will only get called when the browser has the spare time
// to handle animations. This prevents calls from queueing up with old data.
$(window).resize(function () {
    'use strict';
    app.latestWindowSize.height = document.height;
    app.latestWindowSize.width  = document.width;
    requestResize();
});

function requestResize () {
    'use strict';

    if (!app.resizing) {
        app.rAF.request(resizeElements);
    }

    app.resizing = true;
}

function resizeElements () {
    'use strict';

    // Change line height of layouts to vertically align
    var userHeight = $('.layout-user').height() + 'px',
        laptopHeight = $('.layout-laptop').height() + 'px';

    $('.layout-user').css({'line-height': userHeight});
    $('.layout-laptop').css({'line-height': laptopHeight});

    app.resizing = false;
}

$(document).ready(function () {
    'use strict';

    // Head navbar Buttons
    $('#btnHomePage').on('touchend', pushHomePage);
    $('#btnPrevPage').on('touchend', pushPrevPage);
    $('#btnLogout').on('touchend', logout);

    // Tabbar buttons
    $('.btn-device-main-page').on('touchend', showDeviceMainPage);

    // Hide all modals or they will be on top of all content
    $('.modal').hide();

    // Hide buttons based on permission roles
    flushPermissions();
});

function startLoading (id, small) {
    'use strict';
    /* global spinner */

    var target = document.getElementById(id),
        opts   = {
        lines:     small ? 7 : 13, // The number of lines to draw
        length:    small ? 3 : 10, // The length of each line
        width:     small ? 2 : 3,  // The line thickness
        radius:    small ? 2 : 10, // The radius of the inner circle
        corners:   1,              // Corner roundness (0..1)
        rotate:    0,              // The rotation offset
        direction: 1,              // 1: clockwise, -1: counterclockwise
        color:     '#000',         // #rgb or #rrggbb or array of colors
        speed:     1,              // Rounds per second
        trail:     50,             // Afterglow percentage
        shadow:    false,          // Whether to render a shadow
        hwaccel:   true,           // Whether to use hardware acceleration
        className: 'spinner',      // The CSS class to assign to the spinner
        zIndex:    2e9,            // The z-index (defaults to 2000000000)
        top:       'auto',         // Top position relative to parent in px
        left:      'auto'          // Left position relative to parent in px
    };

    new Spinner(opts).spin(target);
}

function pushHomePage (event) {
    'use strict';

    var button       = $('#' + $(event.currentTarget).data('button')),
        page         = $('#' + $(event.currentTarget).data('page')),
        devicePages  = $('.device-pages'),
        homePage     = $('#homePage'),
        headerHeight = 56;

    // Show main page in order to get children element position
    homePage.show();

    var scale        = scaleFromElements(button, devicePages),
        cssScale     = 'scale(' + scale.x + ', ' + scale.y + ')',
        origin       = elementTranslateOriginToElement(button, homePage, scale),
        cssOrigin    = origin.x  + 'px' + ' ' + (origin.y + headerHeight) + 'px',
        translate    = pointDiff({x: origin.x, y: origin.y}, {x: devicePages.width()/2, y: devicePages.height()/2}),
        cssTranslate = 'translate(' + translate.x + 'px, ' + translate.y + 'px)';

    // We need to timeout out the animations or else they will all get set
    // together at the same time.
    setTimeout(function () {
        // Set page scale based on button size
        devicePages.css('transform', cssScale + ' ' + cssTranslate);
        // Animate page scale back to normal
        homePage.css('transform', 'scale(1,1)');
        // Fade in button and fade out page
        setTimeout(function () { button.css({opacity: 1}); }, 200);
        // Hide other pages
        devicePages.css({opacity: 0});
    }, 0);

    // Hide device pages after animation
    setTimeout(function () {
        $('footer').add(page).add(devicePages).hide();
        $(devicePages).add(homePage).attr('style', '');
        $(devicePages).add(homePage).removeClass('animate-all');
    }, 600);

    // Modify header, hide home label, show home button and hide logout button
    showHomePageNavBar();
    // Reset device
    app.device.current = undefined; 
    // Update pages
    app.prevPage = app.currPage;
    app.currPage = 'homePage';
    // Reset main device pate
    $('.tabbar-item').first().trigger('touchend');
    $('.pt-page-current').removeClass('pt-page-current');
    $('#deviceMainPage').addClass('pt-page-current');
}

function pushPrevPage () {
    'use strict';

    $('.pt-pages').show();
    $('#' + app.prevPage).show();
    $('#' + app.currPage).hide();
    $('#btnPrevPage').hide();
    $('#btnHomePage').show();    
}

function pushSecondaryPage (event, pageId, footer) {
    'use strict';

    // Default param values
    pageId = typeof pageId !== 'undefined' ? pageId : '';
    footer = typeof footer !== 'undefined' ? footer : true;

    var button       = $(event.currentTarget),
        page         = $('.device-pages'),
        homePage     = $('#homePage'),
        headerHeight = 56,
        scale        = scaleFromElements(button, page),
        cssScale     = 'scale(' + scale.x + ', ' + scale.y + ')',
        origin       = elementTranslateOriginToElement(button, homePage),
        cssOrigin    = origin.x  + 'px' + ' ' + (origin.y + headerHeight) + 'px',
        translate    = pointDiff({x: origin.x, y: origin.y}, {x: page.width()/2, y: page.height()/2}),
        cssTranslate = 'translate(' + translate.x + 'px, ' + translate.y + 'px)';


    page.css({
        // Set page scale based on button size
        'transform': cssScale + ' ' + cssTranslate,
        // Set page origin relative to button offset
        'transform-origin': cssOrigin,
    });

    cssOrigin = origin.x  + 'px' + ' ' + origin.y + 'px';
    homePage.css('transform-origin', cssOrigin);

    // Fade in page and fade out button
    page.show().css({opacity: 0});

    if (!footer) {
        $('footer').hide();
    }

    if (pageId) {
        $('#' + pageId).show();
        // Hide Device pages
        $('.pt-pages').hide();
        app.prevPage = app.currPage;
        app.currPage = pageId;
    } else { // device main page
        $('.pt-pages').show();
        $('footer').show();
        app.prevPage = app.currPage;
        app.currPage = "deviceMainPage";
    }

    // Wee need to timeout out the animations or else they will all get set
    // together at the same time.
    setTimeout(function () {
        // Set page and button class to animate css changes
        button.addClass('animate-all-fast');
        page.add(homePage).addClass('animate-all');

        page.css({opacity: 1});
        button.css({opacity: 0});

        // Revert page scale
        page.css('transform', 'scale(1)');

        // Scale and zoom out home page
        scale        = scaleFromElements(homePage, button);
        cssScale     = 'scale(' + scale.x + ', ' + scale.y + ')';
        translate    = pointDiff({x: homePage.width()/2, y: homePage.height()/2}, {x: origin.x, y: origin.y});
        translate.x  /= scale.x;
        translate.y  /= scale.y;
        cssTranslate = 'translate(' + translate.x + 'px, ' + translate.y + 'px)';
        homePage.css('transform', cssScale + ' ' + cssTranslate);
    }, 0);

    // Hide logout and show home buttons
    $('#btnHomePage').show().data({
        'button': button.prop('id'),
        'page': pageId
    });
    $('#btnLogout').hide();

    setTimeout(function () {
        homePage.hide();
    }, 600);
}

function scaleFromElements (el1, el2) {
    'use strict';

    return {
        x: el1.outerWidth() / el2.outerWidth(),
        y: el1.outerHeight() / el2.outerHeight()
    };
}

function elementTranslateOriginToElement (el1, el2, scale) {
    'use strict';

    if (!scale) scale = {x:1, y:1};

    return {
        x: (el1.offset().left - el2.offset().left)*scale.x + el1.outerWidth()/2,
        y: (el1.offset().top - el2.offset().top)*scale.y + el1.outerHeight()/2
    };
}

function pointDiff (p1, p2) {
    'use strict';

    return {
        x: p1.x - p2.x,
        y: p1.y - p2.y
    };
}

function showHomePageNavBar () {
    'use strict';

    $('.home-label').show();
    $('#deviceListPopover').text('Select Device');
    $('#btnHomePage').hide();
    $('#btnLogout').show();
    $('#deviceListPopover').hide();
}

function flushPermissions() {
    'use strict';

    var roles       = !$.cookie('roles') ? '' : $.parseJSON($.cookie('roles')),
        homeButtons = $('.btn-home-wrap');

    // Enable everything for admin
    if (roles[0] === 'ROLE_ADMIN') {
        homeButtons.removeClass('hidden');
        return;
    }

    // Then enable the ones passed in the cookie's roles value
    for (var i in roles) {
        switch (roles[i])
        {
        case 'ROLE_DEVICE':
            homeButtons.has('#btnDeviceMainPage').removeClass('hidden');
            break;
        case 'ROLE_CONF':
            homeButtons.has('#btnConferenceControlPage').removeClass('hidden');
            break;
        case 'ROLE_SCRUMS':
            homeButtons.has('#btnItScrumsPage').removeClass('hidden');
            break;
        case 'ROLE_DOOR':
            homeButtons.has('#btnBrivoPage').removeClass('hidden');
            break;
        case 'ROLE_ROOMS':
            homeButtons.has('#btnRoomsPage').removeClass('hidden');
            break;
        }
    }
}

