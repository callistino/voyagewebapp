/**
 * Simple counter class. It will increment/decrement a counter property every
 * interval until stopped.
 */
var Counter = (function () {
    'use strict';
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

    /**
     * Constructor
     *
     * @param number   counter   counter property
     * @param number   increment how much to increment counter each interval
     * @param number   interval  how long in ms to set the interval
     * @param function callback  called at each interval
     */
    function Counter (counter, increment, interval, callback) {
        this.counter   = counter;
        this.increment = increment;
        this.interval  = interval;
        this.callback  = callback;
        this.update    = __bind(this.update, this);
        this.run       = false; // Flag to keep counter running or not
    }

    /**
     * Stop counter from updating by setting the run flag to false
     */
    Counter.prototype.stop = function () {
        return this.run = false;
    };

    /**
     * Start updating counter. Set the run flag to true
     */
    Counter.prototype.start = function () {
        this.run = true;
        return this.update();
    };

    /**
     * Update counter if the run flag is set and set a timeout to call this
     * same method in the future given an interval
     */
    Counter.prototype.update = function () {
        if (this.run) {
            this.counter += this.increment;
            this.callback();
            return setTimeout(this.update, this.interval);
        }
    };

    return Counter;
})();
