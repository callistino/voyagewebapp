/**
 * @author Reinier Pelayo
 *
 */
Config = (function () {
    __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    function Config () {
        this.MCU_IP           = '123.123.123.123';
        this.VCS_ADDRESS      = 'vcs.address.com';
        this.server           = '';
        this.notificationPath = '/getNotification.php';
        this.calendarPath     = '/getCalendarMeetings.php';
        this.calendar         = '/calendar.php';
        this.ciscoPath        = '/sx20Commands.php';
        this.mcuPath          = '/mcuCommands.php';
        this.brivoPath        = '/brivoCommands.php';
        this.authPath         = '/authentication.php';
    }

    return Config;
})();

