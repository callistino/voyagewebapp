<?php

require_once 'vendor/autoload.php';
require_once 'src/FAMC/Cisco/Telepresence/MCUAPI.php';
require_once 'src/FAMC/Cisco/Telepresence/mcuActions.php';
require_once 'src/FAMC/db/VoyagerDB.php';
require_once 'config.php';
require_once 'functions.php';

use FAMC\Cisco\Telepresence\MCUAPI\MCUAPI;

$voyagerDB = new VoyagerDB();
$mcuAPI    = new MCUAPI();

if (isset($_POST['Command'])) {
    $command = checkInput($_POST['Command']);
    if (isset($_POST['participantData'])){
        $participantData = $_POST['participantData'];
    }else{
        $participantData = false;
    }
    switch ($command) {
        case 'GetMCUCallList':
            actionGetMCUCallList($mcuAPI);
            break;
        case 'GetMCUParticipants':
            actionGetMCUParticipants($mcuAPI);
            break;
        case 'ModifyParticipantMute':
            actionModifyParticpantMute($mcuAPI, $participantData);
            break;
        case 'ModifyAllParticipantsMute':
            actionModifyAllParticipantsMute($mcuAPI, $participantData);
            break;
        case 'DisconnectParticipant':
            actionDisconnectParticipant($mcuAPI, $participantData);
            break;
        case 'ModifyParticipantLayout':
            actionModifyParticipantLayout($mcuAPI, $participantData);
            break;
        default:
            echo "Error: command not found!\n";
            print_r($_POST);
            break;
    }
} else {
    echo "Error: Command not specified!\n";
    print_r($_POST);
}

//store user command
if (isset($_POST['Username'], $action)) {
    $userDisplayName = $ldap->user()->info(($_POST['Username']), array('displayname'));
    $action = $userDisplayName[0]['displayname'][0] . " $action";
    $voyagerDB->insertUserEvent($_POST['Username'], $action, checkInput($_POST['deviceIP']));
}
