<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cazlin
 * Date: 9/19/13
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */

require_once 'vendor/autoload.php';
require_once 'src/FAMC/db/VoyagerDB.php';
require_once 'config.php';
require_once 'functions-mobile.php';

use adLDAP\adLDAP;

if (isset($_POST['ID'],$_POST['DeviceIP'])) {
    $voyagerDB = new VoyagerDB();
    $id        = $_POST['ID'];
    $device    = $_POST['DeviceIP'];
    $user      = $_POST['User'];

    while (true) {
        $newEvent = $voyagerDB->getLatestNotificationAfterId($id,$device);

        if (is_array($newEvent)) {
            if (strtolower($user) == strtolower($newEvent['USERNAME'])) {
                continue;
            }
            echo json_encode($newEvent);
            break;
        }
        usleep(500);
    }
} elseif (isset($_POST['GetNotificationHistory'], $_POST['DeviceIP'])) {
    $voyagerDB = new VoyagerDB();
    $list      = $voyagerDB->getNotificationHistory(100,$_POST['DeviceIP']);
    $arr       = array();
    $ldap      = new adLDAP(array(
                    'account_suffix'     => '@'.LDAP_HOST,
                    'domain_controllers' => array(LDAP_HOST),
                    'base_dn'            => '',
                    'ad_port'            => LDAP_PORT
                ));

    foreach($list as $row) {
        $row['EVENT_TIME'] = strtotime($row['EVENT_TIME']);
        if ($row['EVENT_TIME'] > strtotime('today')) { // only show today's notifications
            $displayName = $ldap->user()->info($row['USERNAME'], array('displayname'));
            $row['DISPLAY_NAME'] = $displayName ? $displayName[0]['displayname'][0] : $row['USERNAME'];
            $arr[] = $row;
        }
    }

    if (isset($_POST['mobile'])) {
        echo mobileHTMLNotificationHistory($arr);
    } else {
        echo json_encode($arr);
    }
}

