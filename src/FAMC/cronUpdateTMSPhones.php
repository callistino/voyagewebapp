<?php
chdir(dirname(__FILE__) . '../../');
require_once 'vendor/autoload.php';
require_once 'vendor/lastcraft/simpletest/browser.php';
require_once 'config.php';
require_once 'src/FAMC/db/VoyagerDB.php';

function getSystemsTMS()
{
    $url = 'http://address.com';
    $client = new SoapClient($url, array(
        "login" => "user",
        "password" => "password",
        "trace" => true,
        'soap_version'   => SOAP_1_2));
    $phones = array();
    $devices = $client->GetSystems();
    $devices = $devices->GetSystemsResult->TMSSystem;
    foreach($devices as $dev){
        $ip =  substr($dev->WebInterfaceURL,0,strpos($dev->WebInterfaceURL,'/'));
        echo "$ip\n";
        if (strpos($dev->SystemType,'SX20')){//SX20
            $name = $dev->SystemName;
            $mac = substr($dev->SystemName, strpos($dev->SystemName,'SEP'));
            $mac = substr($mac,0,strpos($mac,')'));
            $ip =  substr($dev->WebInterfaceURL,0,strpos($dev->WebInterfaceURL,'/'));
            $phones[] = array('ip_address'=>$ip,'device_id'=>$mac,'device_description'=>$name);
        }
    }
    return $phones;
}

$voyagerDB = new VoyagerDB();
$phones = getSystemsTMS();

foreach($phones as $phone){
    //print_r($phone);
    $voyagerDB->insertOrUpdatePhone($phone);
}



