<?php

require_once('MySQLConnection.php');

class VoyagerDB extends MySQLConnection
{

    public function __construct()
    {
        parent::__construct(MYSQL_SERVER, MYSQL_DB_USER, MYSQL_DB_PASSWORD, MYSQL_DB_NAME);
    }

    public function insertUserEvent($username, $event, $device)
    {
        $this->query("INSERT INTO events(USERNAME, EVENT, DEVICE)
         VALUES(:username, :event, :device)",
                     array(
                         ':username' => $username,
                         ':event'    => $event,
                         ':device'   => $device
                     ));
    }

    public function getLatestNotificationAfterId($id, $device)
    {
        $stmt  = $this->query("
            SELECT *
            FROM events
            WHERE DEVICE = :device AND ID > :id
            ORDER BY ID DESC
            LIMIT 1",
            array(':device' => $device, ':id' => $id)
        );

        if ($result = $stmt->fetch()) {
            return $result;
        } else {
            return false;
        }
    }

    public function getNotificationHistory($i, $device)
    {
        $stmt = $this->query("SELECT USERNAME, EVENT, EVENT_TIME  FROM events
                                WHERE DEVICE = :device ORDER BY ID DESC LIMIT 0, $i ",
                             array(':device' => $device));

        return $stmt->fetchAll();
    }

    public function getDeviceList()
    {
        $stmt      = $this->query("SELECT * FROM phones");
        $resultset = $stmt->fetchAll();
        $devices   = array();
        foreach ($resultset as $key => $value) {
            $devices[$value["description"]] = $value;
        }

        return $devices;
    }

    public function getRoomList()
    {
        $stmt      = $this->query("SELECT * FROM conferencerooms");
        $resultset = $stmt->fetchAll();

        $devices = array();
        foreach ($resultset as $key => $value) {
            $devices[$value["description"]] = $value;
        }

        return $devices;
    }

    public function getPhones()
    {
        $stmt = $this->query("SELECT * FROM phones");

        return $stmt->fetchAll();
    }

    public function insertOrUpdatePhone($phone)
    {
        if (!is_array($phone)) {
            return false;
        }
        $stmt = $this->query("SELECT * FROM phones WHERE device_id = ?", $phone['device_id']);

        if ($stmt->rowCount() == 1) {
            $stmt = $this->query("UPDATE phones SET ip_address = :ip, device_description = :descrip where device_id = :id",
                                 array(
                                     ':ip'      => $phone['ip_address'],
                                     ':descrip' => $phone['device_description'],
                                     ':id'      => $phone['device_id']
                                 ));
        } else {

            $stmt = $this->query("INSERT INTO phones (ip_address, description, device_id, device_description)
                                    VALUES(:ip, :descrip, :id, :devdesc)", array(
                ':ip'      => $phone['ip_address'],
                ':descrip' => $phone['device_description'],
                ':id'      => $phone['device_id'],
                ':devdesc' => $phone['device_description']
            ));
        }

        return $stmt->fetchAll();
    }

    public function getDeviceDataByIp($ip){
        $stmt = $this->query("SELECT * FROM phones WHERE ip_address = ?", $ip);
        if ($stmt->rowCount() == 1) {
            return $stmt->fetch();
        }else{
            return false;
        }

    }
}
