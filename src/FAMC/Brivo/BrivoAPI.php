<?php
/**
 * Created by PhpStorm.
 * User: cazlin
 * Date: 12/11/13
 * Time: 4:06 PM
 */

require_once '../../vendor/autoload.php';

$browser = new \SimpleBrowser();
$browser->get("https://brivo.com/login/Login.do");
$browser->authenticate("tester", "tester");
$browser->clickLink("Unlock Door");

$data = json_decode($browser->getContent());
    print_r($data->data);

function doCurlHTTP($url, $method = 'GET', $credentials = false, $content = false){

    if(empty($url) || empty($method)){
        return false;
    }
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);

    if ($method == 'POST'){
        //curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, ($content));
    }
    if ($credentials && isset($credentials['username'],$credentials['password'])){
        curl_setopt($ch, CURLOPT_USERPWD, urlencode($credentials['username']) . ':' . urlencode($credentials['password']));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

    $curl['response']     = curl_exec    ( $ch );
    $curl['error']        = curl_errno   ( $ch );
    $curl['errmsg']       = curl_error   ( $ch );
    $curl['header']       = curl_getinfo ( $ch );
    $curl['responseCode'] = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );

    curl_close($ch);
    return $curl;

}
