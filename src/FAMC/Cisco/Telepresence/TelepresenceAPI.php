<?php

namespace FAMC\Cisco\Telepresence\TelepresenceAPI;

/**
 * Class TelepresenceAPI
 *
 * This class houses all the necessary commands to communicate with the Cisco
 * Telepresence enabled devices.
 *
 * @package FAMC\Cisco\Telepresence\TelepresenceAPI
 * @author  Reinier Pelayo <rpelayo@franklinamerican.com>
 */
class TelepresenceAPI
{
    public $device;

    /**
     * This function connects to a device
     * and dials the number
     *
     * @param  string $number phone number or extension]
     *
     * @return xml http response
     */
    public function dialNumber($number)
    {
        $xmlData = "
    <Command>
        <Dial command='True'>
            <Number>$number</Number>
        </Dial>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function sends a clicked key to the device
     *
     * @param  string $key
     * @param  int    $callId
     *
     * @return xml http response
     */
    public function sendKey($key, $callId)
    {
        $xmlData = "
    <Command>
        <DTMFSend command='True'>
            <CallId>$callId</CallId>
            <DTMFString>$key</DTMFString>
        </DTMFSend>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function disconnects a call by ID
     *
     * @param  int $callId
     *
     * @return xml http response
     */
    public function disconnectCall($callId)
    {
        $xmlData = "
    <Command>
        <Call>
            <Disconnect command='True'>
                <CallId>$callId</CallId>
            </Disconnect>
        </Call>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function disconnects all calls
     *
     * @return xml http response
     */
    public function disconnectAllCalls()
    {
        $xmlData = "
    <Command>
        <Call>
        <DisconnectAll command='True'>
        </DisconnectAll>
        </Call>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function starts presentation of
     * connected PC
     *
     * @param  int $source
     *
     * @return xml http response
     */
    public function startPresentation($source = 2)
    {
        $xmlData = "
    <Command>
        <Presentation>
            <Start command='True'>
                <PresentationSource>$source</PresentationSource>
            </Start>
        </Presentation>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function stops presentation of
     * connected PC
     *
     * @return xml http response
     */
    public function stopPresentation()
    {
        $xmlData = "
    <Command>
        <Presentation>
            <Stop command='True'>
            </Stop>
        </Presentation>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function changes the remote layout on a
     * conference
     *
     * @param  int $callId
     *
     * @return xml http response
     */
    public function nextRemoteLayout($callId)
    {
        $xmlData = "
    <Command>
        <DTMFSend command='True'>
            <CallId>$callId</CallId>
            <DTMFString>*152000</DTMFString>
        </DTMFSend>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function moves the camera to a specific position
     *
     * @param  int $camId
     * @param  int $pan
     * @param  int $tilt
     * @param  int $zoom
     *
     * @return xml http response
     */
    public function moveCamToPosition($camId, $pan = false, $tilt = false, $zoom = false)
    {
        $xmlData = "
    <Command>
        <Camera>
            <PositionSet command='True'>
                <CameraId>$camId</CameraId>";
                if($pan) {
                    $xmlData .= "\n<Pan>$pan</Pan>";
                }
                if($tilt) {
                    $xmlData .= "\n<Tilt>$tilt</Tilt>";
                }
                if($zoom) {
                    $xmlData .= "\n<Zoom>$zoom</Zoom>";
                }
            $xmlData .= "
            </PositionSet>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function uto focuses the camera
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function triggerAutoFocus($camId){
        $xmlData = "
        <Command>
            <Camera>
                <TriggerAutofocus command='True'>
                    <CameraId>$camId</CameraId>
                </TriggerAutofocus>
            </Camera>
        </Command>";
        return $this->postCommand($xmlData);
    }

    /**
     * This function pans the camera left
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function moveCamToLeft($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Pan>Left</Pan>
                <PanSpeed>3</PanSpeed>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function pans the camera rigth
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function moveCamToRight($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Pan>Right</Pan>
                <PanSpeed>3</PanSpeed>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function stops panning the camera
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function stopCamPanMovement($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Pan>Stop</Pan>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function tilts the camera up
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function moveCamUp($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Tilt>Up</Tilt>
                <TiltSpeed>3</TiltSpeed>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function tilts the camera right
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function moveCamDown($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Tilt>Down</Tilt>
                <TiltSpeed>3</TiltSpeed>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function stops tilting the camera
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function stopCamTiltMovement($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Tilt>Stop</Tilt>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function zooms in on the camerathe camera in
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function zoomIn($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Zoom>In</Zoom>
                <ZoomSpeed>5</ZoomSpeed>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function zooms the camera out
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function zoomOut($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Zoom>Out</Zoom>
                <ZoomSpeed>5</ZoomSpeed>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function stops the camera zoom
     *
     * @param  int $camId
     *
     * @return xml http response
     */
    public function stopZoom($camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <Ramp command='True'>
                <CameraId>$camId</CameraId>
                <Zoom>Stop</Zoom>
            </Ramp>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function moves the camera to the
     * specified preset number.
     *
     * @param  int $presetNumber
     * @param  int $camId
     *
     * @return xml http response
     */
    public function moveCamToPreset($presetNumber, $camId)
    {
        $xmlData = "
    <Command>
        <Camera>
            <PositionActivateFromPreset command='True'>
                <CameraId>$camId</CameraId>
                <PresetId>$presetNumber</PresetId>
            </PositionActivateFromPreset>
        </Camera>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function wakes up the sx20 from standby
     *
     * @return xml http response
     */
    public function wakeupFromStandby()
    {
        $xmlData = "
    <Command>
        <Standby>
            <Deactivate command='True'>
            </Deactivate>
        </Standby>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function mutes all mics on the device
     *
     * @return xml http response
     */
    public function muteMicrophone()
    {
        $xmlData = "
    <Command>
        <Audio>
            <Microphones>
                <Mute command='True'>
                </Mute>
            </Microphones>
        </Audio>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function ummutes all mics on the device
     *
     * @return xml http response
     */
    public function unmuteMicrophone()
    {
        $xmlData = "
    <Command>
        <Audio>
            <Microphones>
                <Unmute command='True'>
                </Unmute>
            </Microphones>
        </Audio>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function changes the volume
     *
     * @param  int $volume
     *
     * @return xml http response
     */
    public function changeVolume($volume)
    {
        $xmlData = "
    <Configuration>
        <Audio>
          <Volume>$volume</Volume>
        </Audio>
    </Configuration>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function changes the presentation layout
     *
     * @param  string $layout
     *
     * @return xml    http response
     */
    public function changeLayout($layout)
    {
        $xmlData = "
    <Command>
        <Video>
            <PictureLayoutSet command='True'>
                <Target>local</Target>
                <LayoutFamily>$layout</LayoutFamily>
            </PictureLayoutSet>
        </Video>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function displays the self view
     *
     * @return xml http response
     */
    public function showSelfView()
    {
        $xmlData = "
    <Command>
        <Video>
            <Selfview>
                <Set command='True'>
                    <Mode>On</Mode>
                </Set>
            </Selfview>
        </Video>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * This function hides the self view
     *
     * @return xml http response
     */
    public function hideSelfView()
    {
        $xmlData = "
    <Command>
        <Video>
            <Selfview>
                <Set command='True'>
                    <Mode>Off</Mode>
                </Set>
            </Selfview>
        </Video>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * Returns the call history
     *
     * @param string $callHistoryFilter
     * @param int    $callHistoryOffset
     * @param int    $callHistoryLimit
     * @param string $callHistoryDetailLevel
     * @param null   $callHistorySearchString
     *
     * @return xml http response
     */
    public function getCallHistory
    (
        $callHistoryFilter = "All",
        $callHistoryOffset = 0,
        $callHistoryLimit = 10,
        $callHistoryDetailLevel = "Full",
        $callHistorySearchString = null
    )
    {
        $xmlData = "
    <Command>
        <Experimental>
            <CallHistory >
                <Recents command='True'>
                    <Filter>$callHistoryFilter</Filter>
                    <Offset>$callHistoryOffset</Offset>
                    <Limit>$callHistoryLimit</Limit>
                    <DetailLevel>$callHistoryDetailLevel</DetailLevel>
                    <SearchString>$callHistorySearchString</SearchString>
                </Recents>
            </CallHistory >
        </Experimental>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * Returns a list of records after applying the given parameters
     *
     * @param string $searchString
     * @param string $folderId
     * @param string $phonebookType
     * @param int    $offset
     * @param int    $limit
     *
     * @return mixed [xml] [http response]
     */
    public function searchPhonebook
    (
        $searchString = "",
        $folderId = "",
        $phonebookType = "Local", // Local/Corporate
        $offset = 0, // Get records starting from this offset
        $limit = 100
    )
    {
        $xmlData = "
    <Command>
        <Phonebook>
            <Search command='True'>
                <SearchString>$searchString</SearchString>
                <PhonebookType>$phonebookType</PhonebookType>
                <Offset>$offset</Offset>
                <FolderId>$folderId</FolderId>
                <Limit>$limit</Limit>
            </Search>
        </Phonebook>
    </Command>";

        return $this->postCommand($xmlData);
    }

    /**
     * Create and POST command
     *
     * @param  string $command
     *
     * @return bool
     */
    public function postCommand($command)
    {
        $credentials = array('username' => CT_USER, 'password' => CT_PASSWORD);
        $response    = doCurlHTTP($this->getPostUrl($this->device), 'POST', $credentials, $command);

        return $response['response'] ? : false;
    }


    /**
     * Returns the current status
     *
     * @return xml http response
     */
    public function getStatus()
    {
        $params = "Status/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns the current call status
     *
     * @return xml http response
     */
    public function getCallStatus()
    {
        $params = "Status/Call/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns the the current presentation layout
     *
     * @return xml http response
     */
    public function getPresentationLayout()
    {
        $params = "Status/Video/Layout/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns the current volume status
     *
     * @return xml http response
     */
    public function getVolumeStatus()
    {
        $params = "Status/Audio/Volume";

        return $this->getCommandPath($params);
    }

    /**
     * Returns the current presentation status
     *
     * @return xml http response
     */
    public function getPresentationStatus()
    {
        $params = "Status/Conference/Presentation/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns the current microphone status
     *
     * @return xml http response
     */
    public function getMicrophoneStatus()
    {
        $params = "Status/Audio/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns the current selfview status
     *
     * @return xml http response
     */
    public function getSelfviewStatus()
    {
        $params = "Status/Video/Selfview/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns a list of presets from the current device
     *
     * @return xml http response
     */
    public function getPresetList()
    {
        $params = "Status/Preset/";

        return $this->getCommandPath($params);
    }

    /**
     * Returns a snapshot
     *
     * @return string base64 image
     */
    public function getWebSnapshot()
    {
        $this->wakeupFromStandby();

        /**
         * Create a SimpleBrowser object that can fetch a page
         * manipulate the DOM and resubmit without sending the page
         * to the browser
         *
         */
        $browser = new \SimpleBrowser();

        $browser->get("http://" . $this->device . "/web/signin?next=/web/api/snapshot/get_b64?SourceType=localMain");
        $browser->authenticate(CT_USER, CT_PASSWORD);
        $browser->setFieldByName('username', CT_USER);
        $browser->setFieldByName('password', CT_PASSWORD);
        $browser->clickSubmitByName('authform');
        $data = json_decode($browser->getContent());
        if (isset($data->data)) {
            return $data->data;
        } else {
            return false;
        }
    }

    /**
     * Returns a snapshot of the presentation source
     *
     * @return string base64 image
     */
    public function getPresentationSnapshot()
    {
        $this->wakeupFromStandby();

        /**
         * Create a SimpleBrowser object that can fetch a page
         * manipulate the DOM and resubmit without sending the page
         * to the browser
         *
         */
        $browser = new \SimpleBrowser();
        $browser->get("http://" . $this->device . "/web/signin?next=/web/api/snapshot/get_b64?SourceType=localPresentation");
        $browser->authenticate(CT_USER, CT_PASSWORD);
        $browser->setFieldByName('username', CT_USER);
        $browser->setFieldByName('password', CT_PASSWORD);
        $browser->clickSubmitByName('authform');
        $data  = json_decode($browser->getContent());
        $image = $data->data;

        return $image ? : false;
    }

    /**
     * HTTP HET request using curl with the given params as a path string
     *
     * @param string $params url params
     *
     * @return bool
     */
    public function getCommandPath($params)
    {
        $credentials = array('username' => CT_USER, 'password' => CT_PASSWORD);
        $response    = doCurlHTTP($this->getGetUrlPath($this->device) . $params, 'GET', $credentials);

        return $response['response'] ? : false;
    }

    /**
     * HTTP GET request using curl with the given params as an XML string
     *
     * @param $params url params
     *
     * @return bool
     */
    public function getCommandXML($params)
    {
        $credentials = array('username' => CT_USER, 'password' => CT_PASSWORD);
        $response    = doCurlHTTP($this->getGetUrlXML($this->device) . $params, 'GET', $credentials);

        return $response ? : false;
    }


    /**
     * Returns the url
     *
     * @param  string $device device IP Address
     *
     * @return string url
     */
    public function getUrl($device)
    {
        return "http://$device/";
    }

    /**
     * Returns the url and arguments needed to
     * trigger a command through post on the device
     *
     * @param  string $device device IP Address
     *
     * @return string url
     */
    public function getPostUrl($device)
    {
        return "http://$device/putxml";
    }

    /**
     * Returns the url and arguments needed to
     * trigger a command through get on the device
     * using the path of the command.
     *
     * @param  string $device device IP Address
     *
     * @return string url
     */
    public function getGetUrlPath($device)
    {
        return "http://$device/getxml?location=";
    }

    /**
     * Returns the url and arguments needed to
     * trigger a command through get on the device
     * using xml data.
     *
     * @param  string $device device IP Address
     *
     * @return string url
     */
    function getGetUrlXML($device)
    {
        return "http://$device/formputxml?xmldoc=";
    }
}