<?php

namespace FAMC\Cisco\Telepresence\MCUAPI;

/**
 * Class MCUAPI
 *
 * This class houses all the necessary commands to communicate with the Cisco
 * Telepresence MCU video conference system.
 *
 * @package FAMC\Cisco\Telepresence\MCUAPI
 * @author  Cory Azlin <cazlin@franklinamerican.com>
 */
class MCUAPI
{
    /**
     * $globalConnectionCount total number of connections to the MCU
     * @var    int
     * @access public
     */
    public $globalConnectionCount;

    /**
     * Get list of participants on a conference given the dialed number
     *
     * @param int $dialedNumber MCU conference number
     *
     * @return array List of participants
     */
    public function getMCUCallListByDialedNumber ($dialedNumber) {
        $data = array();
        $conferenceNames = $this->getActiveMCUConferences();

        foreach($conferenceNames as $name => $conf) {
            $status = $this->getMCUConferenceStatus($name);

            if ($status['numericId'] == $dialedNumber){
                $data = array('conferenceName' => $name ,
                              'dialedNumber'   => $dialedNumber,
                              'participants'   => $conf);
            }
        }

        $data['globalConnections'] = $this->globalConnectionCount;
        return $data;
    }

    /**
     * Get currently active MCU conferences
     *
     * @return array array of active conferences by name
     */
    public function getActiveMCUConferenceNames () {
        $participants = $this->getActiveMCUParticipants();
        $conferenceNames = array();
        foreach ($participants as $p){
            if (!in_array($p['conferenceName'],$conferenceNames)){
                $conferenceNames[]= $p['conferenceName'];
            }
        }

        return $conferenceNames;
    }

    /**
     * Get currently active MCU conferences
     *
     * @return array Full detailed array of active conferences
     */
    public function getActiveMCUConferences () {
        $participants = $this->getActiveMCUParticipants();
        $conferences = array();
        foreach ($participants as $p){
            $conferences[$p['conferenceName']] []= $p;
        }

        return $conferences;
    }

    /**
     * Get the MCU conference the given participant is on.
     *
     * @param string $byParticipantName name of participant to search
     *
     * @return string|bool Either the found conference or false
     */
    public function getConnectedMCUConferenceByParticipant ($byParticipantName) {
        $participants = $this->getActiveMCUParticipants();
        $conferences = array();

        foreach ($participants as $p) {
            $conferences[$p['conferenceName']] []= $p;
        }

        if ($byParticipantName) {
            foreach ($conferences as $conf) {
                foreach ($conf as $participant) {
                    if ($participant['displayName'] == $byParticipantName) {
                        return $conf;
                    }
                }
            }

            return false;
        }
    }

    /**
     * Get the total number of participants connected to the MCU
     *
     * @return int number of connections
     */
    public function getActiveMCUParticipantCount () {
        $participants = $this->getActiveMCUParticipants();
        return count($participants);
    }

    /**
     * Get the active MCU participants regardles of conference. This is the entry
     * point the provides the needed data for the rest of the functionality on this
     * class.
     *
     * @return array of participants connected to the MCU
     */
    public function getActiveMCUParticipants () {

        $participants = $this->fetchActiveMCUParticipantsFromID();

        foreach ($participants as $participant) {
            $data = array();
            foreach ($participant->member as $attr) {
                $node = $attr->value->children();
                $data[(String)$attr->name] = (String)$node[0];
            }
            $participantsArray[] = $data;
        }

        self::setGlobalConnectionCount(count($participants));

        return $participantsArray;
    }

    /**
     * Fetches all the active MCU participants recursively. The MCU only returns
     * participants in batches of 9 together with an ID to send with the next
     * fetch.
     *
     * @param string $id index used in the recursion.
     *
     * @return array|mixed|\SimpleXMLElement[]
     */
    private function fetchActiveMCUParticipantsFromID ($id = "0") {

        $participantEnumerateQuery = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
        <methodCall>
            <methodName>participant.enumerate</methodName>
            <params>
                <param>
                <value>
                    <struct>
                        <member>
                            <name>authenticationUser</name>
                            <value>
                                <string>%s</string>
                            </value>
                        </member>
                        <member>
                            <name>authenticationPassword</name>
                            <value>
                                <string>%s</string>
                            </value>
                        </member>
                        <member>
                            <name>enumerateFilter</name>
                            <value>
                                <string>connected</string>
                            </value>
                        </member>
                        <member>
                            <name>enumerateID</name>
                            <value>
                                <string>$id</string>
                            </value>
                        </member>
                    </struct>
                </value>
                </param>
            </params>
        </methodCall>
EOT;
        $participantEnumerateQuery = sprintf($participantEnumerateQuery, MCU_USER, MCU_PASSWORD);
        $data = doCurlHTTP(MCU_HOST, "POST", false, $participantEnumerateQuery);
        $xml  = simplexml_load_string($data['response']);
        // Filter all <member> elements into an array
        $xmlMembers = $xml->xpath('params/param/value/struct/member');
        if ((string)$xmlMembers[0]->name === 'enumerateID') {
            $id = (string)$xmlMembers[0]->value->string;
            // Only the last member array value are participants
            $xmlMembers = array_pop($xmlMembers);
            // Filter the participants value struct to only get the relevant
            // participant data
            $xmlMembers = $xmlMembers->xpath('value/array/data/value/struct');
            // Recursively call itself until there is no enumerateID value
            $xmlMembers = array_merge($xmlMembers, $this->fetchActiveMCUParticipantsFromID($id));
        } else {
            // No more enumerateID but still pop the last members' array value
            // for participants relevant data.
            $xmlMembers = array_pop($xmlMembers);
            $xmlMembers = $xmlMembers->xpath('value/array/data/value/struct');
        }
        return $xmlMembers;
    }

    /**
     * Setter for globalConnectionCount. No checks.
     *
     * @param int $globalConnectionCount number of connections to the MCU
     */
    private function setGlobalConnectionCount ($globalConnectionCount) {
        $this->globalConnectionCount = $globalConnectionCount;
    }

    /**
     * Get the status of an MCU conference given the conference name.
     *
     * @param string $conferenceDisplayName The name of the MCU conference to
     *                                      search
     *
     * @return array|bool array containing the details of a conference or false
     *                    if the conference was not found.
     */
    public function getMCUConferenceStatus ($conferenceDisplayName) {
        $conferenceStatus= <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
        <methodCall>
          <methodName>conference.status</methodName>
          <params>
            <param>
              <value>
                <struct>
                  <member>
                    <name>authenticationUser</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>authenticationPassword</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>conferenceName</name>
                    <value>
                      <string>$conferenceDisplayName</string>
                    </value>
                  </member>
                </struct>
              </value>
            </param>
          </params>
        </methodCall>
EOT;
        $conferenceStatus = sprintf($conferenceStatus, MCU_USER, MCU_PASSWORD);
        $data = doCurlHTTP(MCU_HOST, "POST", false, $conferenceStatus);
        $query = simplexml_load_string($data['response']);
        $xml  = $query->xpath('params/param/value/struct/member');
        $data = array();

        foreach ($xml as $attr) {
            $node = $attr->value->children();
            $data[(String)$attr->name] = (String)$node[0];
        }

        return $data;
    }

    /**
     * modifies a single participants audioRxMuted or mute
     *
     * @param string $participantName     required for specifying which active participant to mute
     * @param string $conferenceName      required for specifying which conference the participant is active in
     * @param boolean $mute  required for specifying participant is to be muted or un-muted
     *
     * @param string $participantProtocol required for specifying which video or audio system type
     * @param string $participantType     required for specifying which connection type the participant is using
     *
     * @return bool status if successful mute action
     *
     */
    public function modifyParticpantMute ($participant) {
        $conferenceStatus = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
        <methodCall>
          <methodName>participant.modify</methodName>
          <params>
            <param>
              <value>
                <struct>
                  <member>
                    <name>authenticationUser</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>authenticationPassword</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>conferenceName</name>
                    <value>
                        <string>{$participant['conferenceName']}</string>
                    </value>
                  </member>
                  <member>
                    <name>participantProtocol</name>
                    <value>
                        <string>{$participant['participantProtocol']}</string>
                    </value>
                  </member>
                  <member>
                    <name>participantType</name>
                    <value>
                        <string>{$participant['participantType']}</string>
                    </value>
                    </member>
                  <member>
                    <name>participantName</name>
                    <value>
                      <string>{$participant['participantName']}</string>
                    </value>
                  </member>
                  <member>
                    <name>operationScope</name>
                    <value>
                        <string>activeState</string>
                    </value>
                  </member>
                  <member>
                    <name>audioRxMuted</name>
                    <value>
                        <boolean>{$participant['audioRxMuted']}</boolean>
                    </value>
                  </member>
                </struct>
              </value>
            </param>
          </params>
        </methodCall>
EOT;
        $conferenceStatus = sprintf($conferenceStatus, MCU_USER, MCU_PASSWORD);
        $data = doCurlHTTP(MCU_HOST, "POST", false, $conferenceStatus);
        $response = simplexml_load_string($data['response']);
        return $this->checkMethodResponse($response);
    }

    /**
     * modifies a single participants endpoint video layout
     *
     * @param string $participant   required for specifying which active participant to
     * @param string $layout     required for specifying which active participant to

     * @return bool status if successful mute action
     *
     */
    public function modifyParticipantLayout ($participant) {
        $conferenceStatus= <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
        <methodCall>
          <methodName>participant.modify</methodName>
          <params>
            <param>
              <value>
                <struct>
                  <member>
                    <name>authenticationUser</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>authenticationPassword</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>conferenceName</name>
                    <value>
                        <string>{$participant['conferenceName']}</string>
                    </value>
                  </member>
                  <member>
                    <name>participantProtocol</name>
                    <value>
                        <string>{$participant['participantProtocol']}</string>
                    </value>
                  </member>
                  <member>
                    <name>participantType</name>
                    <value>
                        <string>{$participant['participantType']}</string>
                    </value>
                    </member>
                  <member>
                    <name>participantName</name>
                    <value>
                      <string>{$participant['participantName']}</string>
                    </value>
                  </member>
                  <member>
                    <name>operationScope</name>
                    <value>
                        <string>activeState</string>
                    </value>
                  </member>
                  <member>
                    <name>cpLayout</name>
                    <value>
                        <string>{$participant['cpLayout']}</string>
                    </value>
                  </member>
                </struct>
              </value>
            </param>
          </params>
        </methodCall>
EOT;
        $conferenceStatus = sprintf($conferenceStatus, MCU_USER, MCU_PASSWORD);
        $data = doCurlHTTP(MCU_HOST, "POST", false, $conferenceStatus);
        $response = simplexml_load_string($data['response']);
        return $this->checkMethodResponse($response);
    }

    /**
     * disconnects a single participant from an MCU hosted conference
     *
     * @param string $participant    required for specifying which active participant to
     * @return bool status if successful mute action
     *
     */
    public function disconnectParticipant ($participant) {
        $conferenceStatus = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
        <methodCall>
          <methodName>participant.disconnect</methodName>
          <params>
            <param>
              <value>
                <struct>
                  <member>
                    <name>authenticationUser</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>authenticationPassword</name>
                    <value>
                      <string>%s</string>
                    </value>
                  </member>
                  <member>
                    <name>conferenceName</name>
                    <value>
                        <string>{$participant['conferenceName']}</string>
                    </value>
                  </member>
                  <member>
                    <name>participantProtocol</name>
                    <value>
                        <string>{$participant['participantProtocol']}</string>
                    </value>
                  </member>
                  <member>
                    <name>participantType</name>
                    <value>
                        <string>{$participant['participantType']}</string>
                    </value>
                    </member>
                  <member>
                    <name>participantName</name>
                    <value>
                      <string>{$participant['participantName']}</string>
                    </value>
                  </member>
                </struct>
              </value>
            </param>
          </params>
        </methodCall>
EOT;
        $conferenceStatus = sprintf($conferenceStatus, MCU_USER, MCU_PASSWORD);
        $data = doCurlHTTP(MCU_HOST, "POST", false, $conferenceStatus);
        $response = simplexml_load_string($data['response']);
        return $this->checkMethodResponse($response);
    }

    /**
     * evaluates an MCU API response as an simplexml_load_string
     *
     * @param simplexml_load_string $response required
     *
     * @return bool|SimpleXMLObj true if successful, xml error response or false
     *
     */
    private function checkMethodResponse($response){
        try{
            $string = $response->xpath('params/param/value/struct/member/value/string');
            if($string === 'operation successful'){
                return true;
            }else{
                return false;
            }
        }catch (Exception $e){
            try{
                return $response->xpath('fault/value/struct/member');
            }catch (Exception $e){
                return $e;
            }
        }

    }
}

