<?php
/**
 * SX20 post xml actions
 */
/**
 * @param $cisco
 *
 * @return string
 */
function actionCall($cisco)
{
    $action   = "called " . checkInput($_POST['callNumber']);
    $response = new \SimpleXMLElement(
        $cisco->dialNumber(checkInput($_POST['callNumber']))
    );
    echo str_replace("@", "", json_encode($response));
    return $action;
}

/**
 * @param $cisco
 */
function actionKeyClick($cisco)
{
    $response = new \SimpleXMLElement(
        $cisco->sendKey(
            checkInput($_POST['key']),
            checkInput($_POST['callId'])
        )
    );
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionDisconnect($cisco)
{
    $response = new \SimpleXMLElement(
        $cisco->disconnectCall(checkInput($_POST['callId']))
    );
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionDisconnectAll($cisco)
{
    $response = new \SimpleXMLElement($cisco->disconnectAllCalls());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionCamPosition($cisco,$db)
{
    $device = $db->getDeviceDataByIp($cisco->device);
    $action   = "moved the camera to position";
    if ($device['cam_model'] == 12){
        $response = new \SimpleXMLElement(
            $cisco->moveCamToPosition(
                checkInput($_POST['camId']),
                checkInput($_POST['Pan']),
                checkInput($_POST['Tilt']),
                checkInput($_POST['Zoom'])
            )
        );
    }else{
        //stupid work around for 4x cams
        $response = new \SimpleXMLElement(
            $cisco->moveCamToPosition(
                checkInput($_POST['camId']),
                false,false,
                checkInput($_POST['Zoom'])
            )
        );
        $response .= new \SimpleXMLElement(
            $cisco->moveCamToPosition(
                checkInput($_POST['camId']),
                checkInput($_POST['Pan']),
                checkInput($_POST['Tilt'])
            )
        );
        $response .= new \SimpleXMLElement(
            $cisco->triggerAutoFocus(
                checkInput($_POST['camId'])
            )
        );
    }
    echo str_replace("@", "", json_encode($response));
    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionMoveCamToLeft($cisco)
{
    $action   = "moved the camera to the left";
    $response = new \SimpleXMLElement(
        $cisco->moveCamToLeft(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionMoveCamToRight($cisco)
{
    $action   = "moved the camera to the right";
    $response = new \SimpleXMLElement(
        $cisco->moveCamToRight(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 */
function actionStopCamPanMovement($cisco)
{
    $response = new \SimpleXMLElement(
        $cisco->stopCamPanMovement(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionMoveCamUp($cisco)
{
    $action   = "moved the camera up";
    $response = new \SimpleXMLElement(
        $cisco->moveCamUp(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionMoveCamDown($cisco)
{
    $action   = "moved the camera down";
    $response = new \SimpleXMLElement(
        $cisco->moveCamDown(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 */
function actionStopCamTiltMovement($cisco)
{
    $response = new \SimpleXMLElement(
        $cisco->stopCamTiltMovement(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionZoomIn($cisco)
{
    $action   = "zoomed in";
    $response = new \SimpleXMLElement(
        $cisco->zoomIn(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionZoomOut($cisco)
{
    $action   = "zoomed out";
    $response = new \SimpleXMLElement(
        $cisco->zoomOut(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 */
function actionStopZoom($cisco)
{
    $response = new \SimpleXMLElement(
        $cisco->stopZoom(checkInput($_POST['camId']))
    );
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionMoveToPreset($cisco)
{
    $action   = "moved the camera to preset " . checkInput($_POST['presetNumber']);
    $response = new \SimpleXMLElement(
        $cisco->moveCamToPreset(
            checkInput($_POST['presetNumber']),
            checkInput($_POST['camId'])
        )
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 * @param $response
 */
function actionGetPresetList($cisco)
{
    $response = array();
    $presets = simplexml_load_string($cisco->getPresetList());

    $i = 0;
    foreach ($presets->Preset as $preset) {
        if ($preset->Defined == "True") {
            $response[$i] = ucname($preset->Description);
            $i++;
        }
    }

    if (isset($_POST['mobile'])) {
        echo mobileHTMLPresetsList($response);
    } else {
        echo json_encode($response);
    }
}

/**
 * @param $cisco
 */
function actionGetStatus($cisco)
{
    $response = new \SimpleXMLElement($cisco->getStatus());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionGetCallStatus($cisco)
{
    $response = new \SimpleXMLElement($cisco->getCallStatus());
    echo str_replace("@", "", json_encode($response));
}


/**
 * @param $cisco
 */
function actionGetCallHistory($cisco)
{
    $response = new \SimpleXMLElement($cisco->getCallHistory());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionGetPresentationLayout($cisco)
{
    $response = new \SimpleXMLElement($cisco->getPresentationLayout());
    $response->registerXPathNamespace("ns", "http://www.tandberg.com/XML/CUIL/2.0");
    $layout = $response->xpath("//ns:FamilyName");
    echo str_replace("@", "", json_encode(array('FamilyName' => (string) $layout[0])));
}

/**
 * @param $cisco
 */
function actionGetFavorites($cisco)
{
    $favorites = array();
    // The local phonebook list is treated as the favorites list.
    $response = new \SimpleXMLElement($cisco->searchPhonebook());
    // Create a list of contacts' name with their respective numbers
    foreach ($response->PhonebookSearchResult->ResultSet->Contact as $key => $value) {
        $favorites["$value->Name"] = (string) $value->ContactMethod->Number;
    }
    if (isset($_POST['mobile'])) {
        echo mobileHTMLFavoritesList($favorites);
    } else {
        echo str_replace("@", "", json_encode($favorites));
    }
}

/**
 * @param $cisco
 */
function actionUpdateSnapshot($cisco)
{
    $file = "/tmp/" . checkInput($_POST['deviceIP']) . ".tmp";
    if (file_exists($file) && filemtime($file) >= (time() - 3)) {
        $response = file_get_contents($file);
    } else {
        $response = $cisco->getWebSnapshot();
        file_put_contents($file, $response);
    }
    if (isset($_POST['mobile'])) {
        echo "data:image/png;base64,$response";
    } else {
        echo base64_decode(chunk_split($response));
    }
}

/**
 * @param $cisco
 */
function actionUpdateVolume($cisco)
{
    $response = new \SimpleXMLElement($cisco->getVolumeStatus());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionUpdatePresentation($cisco)
{
    $response = new \SimpleXMLElement($cisco->getPresentationStatus());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionUpdateMicrophones($cisco)
{
    $response = new \SimpleXMLElement($cisco->getMicrophoneStatus());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 */
function actionUpdateSelfview($cisco)
{
    $response = new \SimpleXMLElement($cisco->getSelfviewStatus());
    echo str_replace("@", "", json_encode($response));
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionStartPresentation($cisco)
{
    $action   = "started the presentation ";
    $response = new \SimpleXMLElement(
        $cisco->startPresentation(checkInput($_POST['PresentationSource']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionStopPresentation($cisco)
{
    $action   = "stopped the presentation ";
    $response = new \SimpleXMLElement($cisco->stopPresentation());
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionNextRemoteLayout($cisco)
{
    $action   = "changed the device layout ";
    $response = new \SimpleXMLElement(
        $cisco->nextRemoteLayout(checkInput($_POST['callId']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionMute($cisco)
{
    $action   = "muted the microphone";
    $response = new \SimpleXMLElement($cisco->muteMicrophone());
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionUnmute($cisco)
{
    $action   = "enabled the microphone";
    $response = new \SimpleXMLElement($cisco->unmuteMicrophone());
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionChangeVolume($cisco)
{
    $action   = "changed the volume";
    $response = new \SimpleXMLElement(
        $cisco->changeVolume(checkInput($_POST['volume']))
    );
    echo str_replace("@", "", json_encode($response));

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionChangeLayout($cisco)
{
    $action   = "changed the layout";
    $response = new \SimpleXMLElement(
        $cisco->changeLayout(checkInput($_POST['layout']))
    );
    echo $response;

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionShowSelfview($cisco)
{
    $action   = "enabled self view";
    $response = new \SimpleXMLElement($cisco->showSelfView());
    echo $response;

    return $action;
}

/**
 * @param $cisco
 *
 * @return string
 */
function actionHideSelfview($cisco)
{
    $action   = "disabled self view";
    $response = new \SimpleXMLElement($cisco->hideSelfView());
    echo $response;

    return $action;
}

/**
 * @param $voyagerDB
 */
function actionGetDeviceList($voyagerDB)
{
    updatePhonesByTMS();
    $devices = $voyagerDB->getDeviceList();

    if (isset($_POST['mobile'])) {
        echo mobileHTMLDeviceList($devices);
    } else {
        echo json_encode($devices);
    }
}

/**
 * @param $voyagerDB
 */
function actionGetRoomsList($voyagerDB)
{
    $devices = $voyagerDB->getRoomList();
    if (isset($_POST['mobile'])) {
        echo mobileHTMLRoomsList($devices);
    } else {
        echo json_encode($devices);
    }
}
/**
 * Returns the current conference id
 *
 * @return string conference id
 */
function getConfId()
{
    return (isset($_POST['confId'])) ? checkInput($_POST['confId']) : "";
}

/* TODO: Move this functions to a general html markup generation class */

/**
 * Returns an html formatted string with a list of devices.
 *
 * @param array $devices
 *
 * @return string html formatted list of devices
 */
function mobileHTMLDeviceList($devices)
{
    $html = "<ul class='list-group device-list-group'>";
    foreach ($devices as $deviceName => $attributes) {
        $html .=
            "<li class='list-group-item' data-device-data='" . json_encode($attributes) . "'>
                $deviceName
            </li>";
    }

    return $html . "</ul>";
}

/**
 * Returns an html formatted string with a list of devices.
 *
 * @param array $rooms
 *
 * @return string html formatted list of rooms
 */
function mobileHTMLRoomsList($rooms)
{
    $html = "<ul class='list-group'>";
    foreach ($rooms as $room) {
        $html .=
            "<li id='$room[room_id]' class='list-group-item btn-location'>
                $room[description]
                <button type='button' class='btn btn-add-meeting pull-right' data-room-id='".$room[room_id]."'>
                    <span class='icon-add'></span>
                </button>
            </li>";
    }

    return $html . "</ul>";
}

/**
 * Returns an html formatted string with a list of favorite numbers.
 *
 * @param array $favorites
 *
 * @return string html formatted list of favorites
 */
function mobileHTMLFavoritesList($favorites)
{
    $html = "<ul class='list-group'>";
    foreach ($favorites as $name => $number) {
        $html .=
            "<li class='list-group-item row' onclick='onFavoriteClick(this)'>
                <div class='col-xs-4 favorite-name'>$name</div>
                <div class='col-xs-8 pull-right favorite-number'>$number</div>
            </li>";
    }

    return $html . "</ul>";
}

/**
 * Returns an html formatted string with a list of camera presets
 *
 * @param array $presets
 *
 * @return string html formatted list of presets
 */
function mobileHTMLPresetsList($presets)
{
    if (empty($presets)) {
        return '';
    }

    $html = "<ul class='list-group'>";
    foreach ($presets as $preset) {
        $html .=
            "<li class='list-group-item row' onclick='onPresetClick(this)'>
                <div class='preset-name'>$preset</div>
            </li>";
    }

    return $html . "</ul>";
}


