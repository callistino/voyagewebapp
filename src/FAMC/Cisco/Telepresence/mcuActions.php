<?php

/**
 * @param $participant
 */
function validateParticipantData($participant){
    if(!is_array($participant)){
        return false;
    }
    $requiredAttributes = array(
        'participantName',
        'participantType',
        'participantProtocol',
        'conferenceName'
    );
    foreach($requiredAttributes as $attribute){
        if (!isset($participant[$attribute])) {
            return false;
        }
    }
    return true;
}

/**
 * @param $mcuAPI
 */
function actionGetMCUCallList($mcuAPI)
{
    $dialed = checkInput($_POST['dialedNumber']);
    echo json_encode($mcuAPI->getMCUCallListByDialedNumber($dialed));
}

/**
 * @param $mcuAPI
 */
function actionGetMCUParticipants($mcuAPI)
{
    echo json_encode($mcuAPI->getActiveMCUParticipants());
}

/**
 * @param $mcuAPI
 * @param $participant
 */
function actionModifyParticpantMute($mcuAPI, $participant){
    if (validateParticipantData($participant)){
        echo json_encode($mcuAPI->modifyParticpantMute($participant));
    }
}

/**
 * @param $mcuAPI
 * @param $participants
 */
function actionModifyAllParticipantsMute($mcuAPI, $participants){
    foreach($participants as $participant){
        if (validateParticipantData($participant)){
            $output [$participant['participantName']]= $mcuAPI->modifyParticpantMute($participant);
        }
    }
    echo json_encode($output);
}

/**
 * @param $mcuAPI
 * @param $participant
 */
function actionDisconnectParticipant($mcuAPI, $participant){
    if (validateParticipantData($participant)){
        echo json_encode($mcuAPI->disconnectParticipant($participant));
    }
}

/**
 * @param $mcuAPI
 * @param $participant
 */
function actionModifyParticipantLayout($mcuAPI, $participant){
    if (validateParticipantData($participant)){
        echo json_encode($mcuAPI->disconnectParticipant($participant));
    }
}