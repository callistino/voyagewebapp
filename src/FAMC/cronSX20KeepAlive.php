<?php
chdir(dirname(__FILE__) . '/../../');
require_once 'vendor/autoload.php';
require_once 'vendor/lastcraft/simpletest/browser.php';
require_once 'src/FAMC/Cisco/Telepresence/TelepresenceAPI.php';
require_once 'src/FAMC/Cisco/Telepresence/sx20Actions.php';
require_once 'src/FAMC/Cisco/Telepresence/MCUAPI.php';
require_once 'src/FAMC/db/VoyagerDB.php';
require_once 'config.php';
require_once 'functions.php';

use FAMC\Cisco\Telepresence\TelepresenceAPI\TelepresenceAPI;
$tAPI      = new TelepresenceAPI();
$voyagerDB = new VoyagerDB();
$devices = $voyagerDB->getPhones();
echo "-wake up sx20 devices-\n";
foreach($devices as $row){
    $tAPI->device = $row['ip_address'];
    echo $row['ip_address'];
    $status = $tAPI->wakeupFromStandby();
    if($status){
        echo " - success\n";
    }else{
        echo " - failed\n";
    }
}