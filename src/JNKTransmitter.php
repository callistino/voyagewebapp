<?php

define("kJNKReturnUnencrypted", true);

/**
* (C) 2012 Jan-Niklas Kohlhaas
* v0.3
* issues: cannot send back encrypted data from PHP
*/
class JNKTransmitter
{

	private $decryptionKey;

	function __construct($decryptionKey=NULL) {
		if ($decryptionKey != "") {
			$this->decryptionKey = $decryptionKey;
		}
	}

	public function input() {
		$headers = getallheaders();
		$output = NULL;

		if (isset($headers["Content-Type"]) && isset($headers["jnk-transmission-encrypted"]) && isset($headers["jnk-transmission-version"])) {
			$content = $headers["Content-Type"];
			$encrypted = $headers["jnk-transmission-encrypted"];
			$version = $headers["jnk-transmission-version"];

			if ($content == "application/jnk-transmission") {
				$input = file_get_contents("php://input");
				$input = base64_decode($input);
				if (!$encrypted) {
					$output = json_decode($input,true);
					return $output;
				}else {
					$input = json_decode($input,true);
					foreach ($input as $key => $value) {
						$data = $value["v"];
						$hash = $value["h"];
						$type = $value["t"];
						$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
						$iv = mcrypt_create_iv($size, MCRYPT_RAND);
						$res_non = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->decryptionKey, base64_decode($data), MCRYPT_MODE_ECB, $iv);

						$decrypted = $res_non;
						$dec_s2 = strlen($decrypted);
						$padding = ord($decrypted[$dec_s2-1]);
						$decrypted = substr($decrypted, 0, -$padding);

						if ($type == 1) {
							$output[$key] = json_decode($decrypted,true);
						}else {
							$output[$key] = $decrypted;
						}
					}
					return $output;
				}
			}
		}
	}

	public function output($array=array()) {
		$headers = getallheaders();
		if (isset($headers["Content-Type"])) {
			$content = $headers["Content-Type"];
			if ($content == "application/jnk-transmission") {
				header("Content-Type: application/jnk-transmission-response");
				if ($this->decryptionKey == NULL || kJNKReturnUnencrypted || count($array) == 0) {
					header("jnk-transmission-encrypted: 0");
					printf(base64_encode(json_encode($array)));
				}else {
					header("jnk-transmission-encrypted: 1");
					printf(base64_encode(json_encode($this->createEncryptedArray($array))));
				}
			}
		}
	}

	// Not working yet
	private function createEncryptedArray($array) {
		$output = array();
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$val = json_encode($value);
				$pad = 16 - (strlen($val) % 16);
				$val .= str_repeat(chr(14), $pad);
				$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
				$iv = mcrypt_create_iv($size, MCRYPT_RAND);
				$result = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->decryptionKey, $val, MCRYPT_MODE_ECB, $iv);
				$output[$key]["h"] = md5($val);
				$output[$key]["t"] = 1;
				$output[$key]["v"] = base64_encode(json_encode($value));
			}else {
				$val = $value;
				$pad = 16 - (strlen($value) % 16);
				$val .= str_repeat(chr(14), $pad);
				$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
				$iv = mcrypt_create_iv($size, MCRYPT_RAND);
				$result = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->decryptionKey, $val, MCRYPT_MODE_ECB, $iv);
				$output[$key]["h"] = md5($val);
				$output[$key]["t"] = 2;
				$output[$key]["v"] = base64_encode(ppEncrypt($value,$this->decryptionKey));
			}
		}

		return $array;
	}

}

function ppEncrypt($sValue,$key) {
		$pad = 16 - (strlen($sValue) % 16);
		$sValue .= str_repeat(chr(14), $pad);
		$size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($size, MCRYPT_RAND);
		$result = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $sValue, MCRYPT_MODE_ECB, $iv);
        return base64_encode($result);
}

?>