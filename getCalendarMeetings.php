<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

// TODO restructure zimbra calendar features and clean this shit up

require_once './vendor/autoload.php';
require_once 'authentication.php';
require_once 'functions.php';
require_once 'src/FAMC/db/VoyagerDB.php';
require_once 'config.php';
require_once 'functions-mobile.php';

use Sabre\VObject;
use \adLDAP;
$voyagerDB = new VoyagerDB();
$ad_ldap   = new  \adLDAP\adLDAP( array(
                                     'account_suffix'     => '@' . LDAP_HOST,
                                     'domain_controllers' => array(LDAP_HOST),
                                     'base_dn'            => '',
                                     'ad_port'            => LDAP_PORT
                                  ) );

$ad_ldap->authenticate(LDAP_USERNAME, LDAP_PASSWORD);

/**
 * Post Actions
 */
if (isset($_POST['GetRoomList'])) {
    $devices = $voyagerDB->getRoomList();
    echo json_encode($devices);
}
if (isset($_POST['GetMeetings']) && isset($_POST['FromUser'])) {
    $meetings = getMeetings();
    $events   = parseMeetings($meetings);

    // Sort meetings by start time then by duration
    usort($events, function ($a, $b) {
        if ($a['DTSTART'] === $b['DTSTART']) {
            if ($a['DURATION'] === $b['DURATION']) {
                return 0;
            }
            return ($a['DURATION'] < $b['DURATION']) ? -1 : 1;
        }
        return ($a['DTSTART'] < $b['DTSTART']) ? -1 : 1;
    });
    echo json_encode($events);
}
if (isset($_POST['GetScrums']) && isset($_POST['FromUser'])) {
    $meetings = getMeetings();
    $events   = parseMeetings($meetings, true);
    echo json_encode($events);
}
if (isset($_POST['GetAllScrums'])) {
    //SCRUM CALENDAR USERS MUST BE ADDED TO it.pm DISTRIBUTION LIST
    //IN ORDER FOR VOYAGER AUTOMAGIC CALENDAR TO SHOW THEM
    // TODO create zimbra admin function to automatically share users in it.pm dist calendars with voyager zimbra
    $soapClientA = new \Zimbra\ZCS\SoapClient("address.com", "7071", "user", "pass");
    $user        = new \Zimbra\ZCS\User\Account($soapClientA);
    $xml         = $user->getDistributionListMembersRequest('it.pm');
    $events      = array();
    foreach ($xml->GetDistributionListMembersResponse->dlm as $email) {
        $email             = (String) $email;
        $output            = preg_replace("/(.*)@.*/", "$1", $email); //only username, no domain
        $_POST['FromUser'] = $output;
        $meetings          = getMeetings();
        try {
            $new = parseMeetings($meetings, true);
            foreach ($new as $key => $arr){
                $events[$key] = $arr;
            }
        } catch (Exception $e) {

        }
    }
    if (isset($_POST['mobile'])) {
        echo mobileHTMLTeamScrums($events);
    } else {
        echo json_encode($events);
    }
}
if (isset($_POST['DeleteEventInstance'])) {
    deleteEvent(getEventUID(), getEventDate());
}
if (isset($_POST['DeleteEventSeries'])) {
    deleteEvent(getEventUID());
}
if (isset($_POST['GetAllContacts'])) {
    $usernames = $ad_ldap->user()->find(false, "displayName", "rpe*");

    $users = array();
    foreach ($usernames as $username) {
        $userInfo = $ad_ldap->user()->infoCollection($username);
        if ($userInfo->mail) {
            $users[$username] = $userInfo->mail;
        }
    }
    print_r($users);
}
if (isset($_POST['GetCurrentNextMeeting']) && isset($_POST['FromUser'])) {
    $meetings = getMeetings();
    $events   = parseMeetings($meetings);
    if (count($events) < 1){
        echo 'false';
    }else{
        // Sort meetings by start time then by duration
        usort($events, function ($a, $b) {
            if ($a['DTSTART'] === $b['DTSTART']) {
                if ($a['DURATION'] === $b['DURATION']) {
                    return 0;
                }
                return ($a['DURATION'] < $b['DURATION']) ? -1 : 1;
            }
            return ($a['DTSTART'] < $b['DTSTART']) ? -1 : 1;
        });
        $currentEvent = false;
        $nextEvent = array_pop($events);
        foreach($events as $event){
            if ($event['DTSTART'] > time() && $event['DTEND'] < $nextEvent['DTEND']){
                $nextEvent = $event;
            }
            if ($event['DTSTART'] <= time() && $event['DTEND'] >= time()){
                $currentEvent = $event;
            }
        }
        if ($currentEvent){
            $returnData['currentMeetingEnd'] = date('g:i a', $currentEvent['DTEND']);
            $returnData['currentMeetingStart']  = date('g:i a', $currentEvent['DTSTART']);
            $returnData['currentMeetingTitle']  = $currentEvent['SUMMARY'];
            $returnData['currentMeetingUnixTime'] = $currentEvent['DTEND'];
            $returnData['currentMeetingPhoneNumber'] = findPhoneNumber($currentEvent['DESCRIPTION']);
        }
        $returnData['nextMeetingEnd']   = date('g:i a', $nextEvent['DTEND']);
        $returnData['nextMeetingTitle'] = $nextEvent['SUMMARY'];
        $returnData['nextMeetingStart'] = date('g:i a', $nextEvent['DTSTART']);
        $returnData['nextMeetingTime'] = date('g:i', $nextEvent['DTSTART']) . ' - ' . date('g:i A', $nextEvent['DTEND']);
        if ($nextEvent['DTSTART'] > time()){
            $returnData['nextMeetingUnixTime'] = $nextEvent['DTSTART'];
        } else if($nextEvent['DTEND'] > time()) {
            $returnData['nextMeetingUnixTime'] = $nextEvent['DTEND'] ;
        }else{
            $returnData['nextMeetingUnixTime'] = false;//strtotime('today 05:00 pm');
        }

        echo json_encode($returnData);
    }
}

/**
 * This function connects to a device
 * and dials the number
 *
 * @param  [string]   $calendar [the calendar folder to search]
 * @param  [DateTime] $start    [the start time for the search]
 * @param  [DateTime] $end      [the end time for the search]
 *
 * @return [xml]                [http response]
 */
function getMeetings()
{
    $start = getStartTime()->format("m/d/Y");
    $end   = getEndTime()->format("m/d/Y");
    return getCommand(getPostUrl() . getZimbraAccount() . "/Calendar/?start=$start&end=$end");
}

/**
 * parses ics objects into one dememtional arrays of meetings so we can access the data easier
 * with optional param $filterByRecurring set to true, filters and returns only recurring meetings
 * @param  [icard]    $meetings           [raw ics calendar object from zimbra]
 * @param  [boolean]  $filterByRecurring  [return only Recurring meetings]
 *
 * @return [array]                        [array of meetings]
 */
function parseMeetings($meetings, $filterByRecurring = false)
{
    $icard = VObject\Reader::read($meetings);

    // Save the rules by UIDs now since they will be erased when the calendar is expanded.
    $uidRules = array();
    foreach ($icard->VEVENT as $vevent) {
        $RRULE = (string) $vevent->RRULE;
        $UID   = (string) $vevent->UID;
        if ( strlen( trim($RRULE) ) > 0 ) {
            $uidRules[$UID] = $RRULE;
            //echo $UID . " --> " . $uidRules[$UID] ." <---> " .$RRULE. "\n";
        }
    }

    // Expand the calendar to a given timeframe to get the recurring and normal
    // meetings occurring within it.
    $icard->expand(getStartTime(), getEndTime());

    $events  = array();
    $setUIDs = array();

    foreach ($icard->VEVENT as $vevent) {
        $UID   = (string) $vevent->UID;
        // Parse location string to get only the readable location
        $location = str_replace('\\', '', (string) $vevent->LOCATION);

        // Default iCal object is on UTC timezone
        $start = $vevent->DTSTART->getDateTime();
        $start->setTimezone(new DateTimeZone('America/Chicago'));

        $end = $vevent->DTEND->getDateTime();
        $end->setTimezone(new DateTimeZone('America/Chicago'));

        $attendees = array();
        foreach ($vevent->ATTENDEE as $att) {
            $firstItem = true;
            $name      = "";
            foreach ($att->parameters() as $param) {
                if ($firstItem) {
                    $name                      = (string) $param;
                    $attendees[$name]["EMAIL"] = (string) $att;
                    $firstItem                 = false;
                }
                $attendees[$name][(string) $param->name] = (string) $param;
            }
        }
        $summary     = (string) $vevent->SUMMARY;
        $organizer   = (string) $vevent->ORGANIZER;
        $description = (string) $vevent->DESCRIPTION;
        $recurring   = $uidRules[$UID];

        $user        = $_POST['FromUser'];

        // Parse organizer string to only get the username to be used to parse meetings for this user.
        $temp      = array();
        preg_match_all('/\:([^\@]+)\@/', strtolower($organizer), $temp);

        // the first item after the parse is the username array which only contains the result
        $organizerUser = $temp[1][0];

        if ($filterByRecurring) {
            if (empty($recurring) || count($attendees) < 2  ) {
                continue;
            }
            if (strpos($summary, 'Bi-weekly') !== false){//still working on a better way to filter these
                continue;
            }

        }

        if ((string) $vevent->DURATION === "") {
            $duration = ($end->getTimestamp() - $start->getTimestamp()) / 60;
        } else {
            $duration = $vevent->DURATION;
        }

        $event = array(
            'SUMMARY'        => $summary,
            'DESCRIPTION'    => $description,
            'LOCATION'       => $location,
            'STATUS'         => (string) $vevent->STATUS,
            'DTEND'          => $end->getTimestamp(),
            'DTEND_HOUR'     => $end->format("H"),
            'DTEND_MINUTE'   => $end->format("i"),
            'DUE'            => (string) $vevent->DUE,
            'DTSTART'        => $start->getTimestamp(),
            'DTSTART_HOUR'   => $start->format("H"),
            'DTSTART_MINUTE' => $start->format("i"),
            'DURATION'       => $duration,
            'FREEBUSY'       => (string) $vevent->FREEBUSY,
            'ORGANIZER'      => (string) $vevent->ORGANIZER,
            'ATTENDEES'      => $attendees,
            'UID'            => $UID,
            'URL'            => (string) $vevent->URL,
            'EXDATE'         => (string) $vevent->EXDATE,
            'RDATE'          => (string) $vevent->RDATE,
            'RRULE'          => $recurring,
            'EXRULE'         => (string) $vevent->EXRULE,
            'ACTION'         => (string) $vevent->ACTION,
            'REPEAT'         => (string) $vevent->REPEAT,
            'TRIGGER'        => (string) $vevent->TRIGGER,
            'CREATED'        => (string) $vevent->CREATED,
            'DTSTAMP'        => (string) $vevent->DTSTAMP,
            'SEQUENCE'       => (string) $vevent->SEQUENCE
        );
        $events[$UID]  = $event;
    }
    return $events;
}

/**
 * This function returns a list of contacts from lDAP.
 *
 * @param  [string] $number [phone number or extension]
 *
 * @return [xml]         [http response]
 */
function getAllContacts($contactDescription = false, $contactSearch = "*", $contactSort = true)
{
    global $adldap;
    authenticate();

    return $adldap->contact()->all($contactDescription, $contactSearch, $contactSort);
}

/**
 * This function deletes an event.
 *
 * @param  [string] $uid [uid of event to delete]
 *
 * @return [xml]         [http response]
 */
function deleteEvent($uid, $eventDate = null)
{
    // Create a soapclient with the correct params of your ZCS server
    $soapClient = new \Zimbra\ZCS\SoapClient(ZIMBRA_HOST, ZIMBRA_PORT, ZIMBRA_USER, ZIMBRA_PASSWORD);

    // Construct a mail account with the soapClient
    $calendar = new \Zimbra\ZCS\Mail\Account($soapClient);

    // Retrieve event by uid. Needed to access the event ID to delete
    $event = $calendar->getAppointment($uid);

    $eventID = (string) $event->appt->attributes()->id;
    $invID   = (string) $event->appt->inv->attributes()->id;

    if (isset($eventDate)) {
        $calendar->cancelAppointent("$eventID-$invID", $eventDate);
    } else {
        $calendar->deleteAppointmentSeries("$eventID-$invID");
    }
}

/**
 * Create and POST Resty object
 *
 * @param  [string] $command [xml post]
 *
 * @return [string]          [xml response]
 */
function getCommand($command)
{
    $credentials = array('username' => ZIMBRA_USER, 'password' => ZIMBRA_PASSWORD);
    $response    = doCurlHTTP($command, 'GET', $credentials);
    return $response['response'] ? : false;
}

/**
 * Returns the zimbra account
 *
 * @return [string] [zimbra acount]
 */
function getZimbraAccount()
{
    return $_POST['FromUser'];
}


/**
 * Returns the url and arguments needed to
 * get calendar info
 *
 * @return [string]         [url]
 */
function getPostUrl()
{
    return "https://zimbra.franklinamerican.com/home/";
}

/**
 * Returns the posted start time
 *
 * @return [string] [start time]
 */
function getStartTime()
{
    if (isset($_POST['Start'])) {
        $startDate = new \DateTime("@" . strtotime($_POST['Start']));
    } else {
        $startDate = new \DateTime();
    }

    $startDate->modify("midnight");
    return $startDate;
}

/**
 * Returns the posted end time
 *
 * @return [string] [end time]
 */
function getEndTime()
{
    if (isset($_POST['End'])) {
        $endDate = new \DateTime("@" . strtotime($_POST['End']));
    } else {
        $endDate = new \DateTime();
        $endDate->add(new DateInterval('P1D'));
    }

    $endDate->modify("midnight");
    return $endDate;
}

/**
 * Returns the posted event UID
 *
 * @return [string] [event UID]
 */
function getEventUID()
{
    return (isset($_POST['UID'])) ? checkInput($_POST['UID']) : "";
}

/**
 * Returns the posted event date
 *
 * @return [string] [event date]
 */
function getEventDate()
{
    return (isset($_POST['EventDate'])) ? checkInput($_POST['EventDate']) : "";
}



/**
 * Parse xml string into an array for
 * easy manipulation
 *
 * @param  [type] $response [description]
 *
 * @return [type]           [description]
 */
function parseResponse($response, $recursive = false)
{
    // Only parse xml into string on the first recursive call
    $xmlObj = $recursive ?
        (array) $response : (array) simplexml_load_string($response);
    $array  = array();
    foreach ($xmlObj as $key => $value) {
        if (get_class($value) == "SimpleXMLElement") {
            $array = parseResponse($value, true);
        } else {
            $array[$key] = $value;
        }
    }

    return $array;
}

/**
 * Find a key value in a an array. Can search
 * deep nested keys in mutltidimensional array.
 *
 * @param  [string] $find     [key to find]
 * @param  [array] $in_array [array to search]
 *
 * @return [string]           [value of the key found]
 */
function arraySearchValueForKey($find, $in_array)
{
    if (is_array($in_array)) {
        foreach ($in_array as $key => $val) {
            if (is_array($val)) {
                arraySearchValueForKey($find, $val);
            } else {
                if (preg_match('/' . $find . '/', $key)) {
                    return $val;
                }
            }
        }
    }

    return false;
}
