<?php
// Allow access-control-cross-origin
header("Access-Control-Allow-Origin: *");

// MySQL constants
define('MYSQL_DB_USER',     '');
define('MYSQL_DB_PASSWORD', '');
define('MYSQL_DB_NAME',     '');
define('MYSQL_SERVER',      '');

// Cisco Telepresence constants
define('CT_USER',     '');
define('CT_PASSWORD', '');

// MCU API
define('MCU_USER',    '');
define('MCU_PASSWORD','');
define('MCU_HOST',    '' . '/RPC2');

// Zimbra constants
define('ZIMBRA_USER',     '');
define('ZIMBRA_PASSWORD', '');
define('ZIMBRA_HOST',     '');
define('ZIMBRA_PORT',     '7071');

// LDAP Login
define('LDAP_HOST',            '');
define('LDAP_USERNAME',        '');
define('LDAP_USERNAME_DOMAIN', '');
define('LDAP_BASE',            '');
define('LDAP_PASSWORD',        '');
define('LDAP_PORT',            '');

// General Constants
define('REMOTE_SCRUM_PREFIX', '');

// RabbitMQ Server
define('RABBITMQ_USER',     '');
define('RABBITMQ_PASSWORD', '');
define('RABBITMQ_HOST',     '');
define('RABBITMQ_PORT',     1234);
define('RABBITMQ_QUEUE',    '');